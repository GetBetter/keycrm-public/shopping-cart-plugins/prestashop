<?php
/**
 * MIT License
 *
 * Copyright (c) 2021 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2021 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade module to version 3.0.2
 *
 * @param \KeyCRM $module
 *
 * @return bool
 */
function upgrade_module_3_0_2($module)
{
    if ('keycrm' != $module->name) {
        return false;
    }

    return $module->registerHook('actionCarrierUpdate')
        && upgrade_module_3_0_2_remove_old_files([
            'keycrm/job/abandonedCarts.php',
            'keycrm/job/export.php',
            'keycrm/job/icml.php',
            'keycrm/job/index.php',
            'keycrm/job/inventories.php',
            'keycrm/job/jobs.php',
            'keycrm/job/missing.php',
            'keycrm/job/sync.php',
            'keycrm/lib/CurlException.php',
            'keycrm/lib/InvalidJsonException.php',
            'keycrm/lib/JobManager.php',
            'keycrm/lib/KeycrmApiClient.php',
            'keycrm/lib/KeycrmApiClientV4.php',
            'keycrm/lib/KeycrmApiClientV5.php',
            'keycrm/lib/KeycrmApiErrors.php',
            'keycrm/lib/KeycrmApiResponse.php',
            'keycrm/lib/KeycrmDaemonCollector.php',
            'keycrm/lib/KeycrmHttpClient.php',
            'keycrm/lib/KeycrmInventories.php',
            'keycrm/lib/KeycrmProxy.php',
            'keycrm/lib/KeycrmService.php',
            'keycrm/public/css/.gitignore',
            'keycrm/public/css/keycrm-upload.css',
            'keycrm/public/js/.gitignore',
            'keycrm/public/js/exec-jobs.js',
            'keycrm/public/js/keycrm-upload.js',
        ]);
}

/**
 * Remove files that was deleted\moved\renamed in new version and currently outdated
 *
 * @param array $files File paths relative to the `modules/` directory
 */
function upgrade_module_3_0_2_remove_old_files($files)
{
    try {
        foreach ($files as $file) {
            if (0 !== strpos($file, 'keycrm/')) {
                continue;
            }

            $fullPath = sprintf(
                '%s/../%s', __DIR__, str_replace('keycrm/', '', $file)
            );

            if (!file_exists($fullPath)) {
                continue;
            }

            KeycrmLogger::writeCaller(
                __METHOD__, sprintf('Remove `%s`', $file)
            );

            unlink($fullPath);
        }

        return true;
    } catch (Exception $e) {
        KeycrmLogger::writeCaller(
            __METHOD__,
            sprintf('Error removing `%s`: %s', $file, $e->getMessage())
        );
    } catch (Error $e) {
        KeycrmLogger::writeCaller(
            __METHOD__,
            sprintf('Error removing `%s`: %s', $file, $e->getMessage())
        );
    }

    return false;
}
