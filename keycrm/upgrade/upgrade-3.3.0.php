<?php
/**
 * MIT License
 *
 * Copyright (c) 2021 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2021 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Upgrade module to version 3.3.0
 *
 * @param \KeyCRM $module
 *
 * @return bool
 */
function upgrade_module_3_3_0($module)
{
    if ('keycrm' != $module->name) {
        return false;
    }

    $oldCustomFolder = _PS_MODULE_DIR_ . '/keycrm_custom/classes';
    $newCustomFolder = _PS_MODULE_DIR_ . '/keycrm/custom/classes';

    if (file_exists($oldCustomFolder)) {
        if (!file_exists($newCustomFolder)) {
            mkdir($newCustomFolder, 0777, true);
        }

        keycrm_upgrade_recursive_copy($oldCustomFolder, $newCustomFolder);
    }

    return true;
}

function keycrm_upgrade_recursive_copy($src, $dst, $childFolder = '')
{
    $dir = opendir($src);

    if (!file_exists($dst)) {
        mkdir($dst);
    }

    if ('' != $childFolder) {
        if (!file_exists($dst . '/' . $childFolder)) {
            mkdir($dst . '/' . $childFolder);
        }

        while (false !== ($file = readdir($dir))) {
            if (('.' != $file) && ('..' != $file)) {
                if (is_dir($src . '/' . $file)) {
                    keycrm_upgrade_recursive_copy($src . '/' . $file, $dst . '/' . $childFolder . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $childFolder . '/' . $file);
                }
            }
        }
    } else {
        while (false !== ($file = readdir($dir))) {
            if (('.' != $file) && ('..' != $file)) {
                if (is_dir($src . '/' . $file)) {
                    keycrm_upgrade_recursive_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
    }

    closedir($dir);
}
