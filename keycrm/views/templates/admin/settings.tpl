{**
 * MIT License
 *
 * Copyright (c) 2020 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2020 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 *}
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/vendor/sumoselect.min.css">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/sumoselect-custom.min.css">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/keycrm-orders.min.css">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/keycrm-upload.min.css">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/keycrm-export.min.css">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/styles.min.css">

{assign var="systemName" value="KeyCRM.app"}
{capture name="catalogTitleName"}{l s='Icml catalog' mod='keycrm'}{/capture}
{assign var="catalogTitleName" value=$smarty.capture.catalogTitleName}

<title>{$systemName|escape:'htmlall':'UTF-8'}</title>
<div class="keycrm keycrm-wrap hidden">
    {include file='./module_messages.tpl'}
    {include file='./module_translates.tpl'}
    <div class="keycrm-container keycrm-column">
        <aside class="keycrm-column__aside">
            <div class="keycrm-menu">
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_connection" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_active"><span>{l s='Connection' mod='keycrm'}<span/></a>
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_delivery_types" class="keycrm-menu__btn keycrm-menu__btn_big{if in_array('delivery', $errorTabs)} keycrm-menu__btn_error{/if}"><span>{l s='Delivery' mod='keycrm'}<span/></a>
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_order_statuses" class="keycrm-menu__btn keycrm-menu__btn_big{if in_array('statuses', $errorTabs)} keycrm-menu__btn_error{/if}"><span>{l s='Order statuses' mod='keycrm'}<span/></a>
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_payment_types" class="keycrm-menu__btn keycrm-menu__btn_big{if in_array('payment', $errorTabs)} keycrm-menu__btn_error{/if}"><span>{l s='Payment types' mod='keycrm'}<span/></a>
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_default_types" class="keycrm-menu__btn keycrm-menu__btn_big{if in_array('deliveryDefault', $errorTabs) || in_array('paymentDefault', $errorTabs)} keycrm-menu__btn_error{/if}"><span>{l s='Default' mod='keycrm'}<span/></a>
{*                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_stock" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_active"><span>{l s='Stock' mod='keycrm'}<span/></a>*}
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_orders_upload" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive"><span>{l s='Upload orders' mod='keycrm'}<span/></a>
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_carts_sync" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive"><span>{l s='Abandoned carts' mod='keycrm'}<span/></a>
{*                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_catalog" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive{if in_array('catalog', $errorTabs)} keycrm-menu__btn_error{/if}"><span>{$catalogTitleName|escape:'htmlall':'UTF-8'}<span/></a>*}
{*                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_daemon_collector" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive"><span>{l s='Daemon Collector' mod='keycrm'}<span/></a>*}
{*                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&item=consultant" data-tab-trigger="rcrm_tab_consultant" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive"><span>{l s='Online consultant' mod='keycrm'}<span/></a>*}
                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" data-tab-trigger="rcrm_tab_advanced" class="keycrm-menu__btn keycrm-menu__btn_big keycrm-menu__btn_inactive keycrm-menu__btn_hidden"><span>{l s='Advanced' mod='keycrm'}<span/></a>
            </div>
        </aside>
        <article class="keycrm-column__content">
            <h1 class="keycrm-title keycrm-title_content">{$systemName|escape:'htmlall':'UTF-8'}</h1>
            <div class="keycrm-form keycrm-form_main">
                <form class="rcrm-form-submit-trigger" action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" method="post" id="submitkeycrm-form">
                    <input type="hidden" name="submitkeycrm" value="1" />
                    <div id="rcrm_tab_connection">
                        <div class="keycrm-form__title">{l s='Connection Settings' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                            <input required type="hidden" name="{$urlName|escape:'htmlall':'UTF-8'}" value="https://openapi.keycrm.app/v1" class="keycrm-form__area" placeholder="{l s='KeyCRM.app URL' mod='keycrm'}">
                        </div>
                        <div class="keycrm-form__row">
                            <input required type="text" name="{$apiKeyName|escape:'htmlall':'UTF-8'}" value="{$apiKey|escape:'htmlall':'UTF-8'}" class="keycrm-form__area" placeholder="{l s='API key' mod='keycrm'}">
                        </div>
                        <div class="keycrm-form__row">
                            <div style="display:none;" class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$enableHistoryUploadsName|escape:'htmlall':'UTF-8'}" value="{$enableHistoryUploads|escape:'htmlall':'UTF-8'}" {if $enableHistoryUploads}checked="checked"{/if} id="historyuploads-active">
                                <label for="historyuploads-active">{l s='Enable history uploads' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div class="keycrm-form__title"
                             style="margin-top: 40px;">{l s='Order number' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                            <div class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$enableOrderNumberSendingName|escape:'htmlall':'UTF-8'}" value="{$enableOrderNumberSending|escape:'htmlall':'UTF-8'}" {if $enableOrderNumberSending}checked="checked"{/if} id="sendnumbers-active">
                                <label for="sendnumbers-active">{l s='Send order number to KeyCRM.app' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div class="keycrm-form__row">
                            <div style="display:none;" class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$enableOrderNumberReceivingName|escape:'htmlall':'UTF-8'}" value="{$enableOrderNumberReceiving|escape:'htmlall':'UTF-8'}" {if $enableOrderNumberReceiving}checked="checked"{/if} id="receivenumbers-active">
                                <label for="receivenumbers-active">{l s='Receive order number from KeyCRM.app' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div style="display:none;" class="keycrm-form__title"
                             style="margin-top: 40px;">{l s='Corporate clients' mod='keycrm'}</div>
                        <div style="display:none;" class="keycrm-form__row">
                            <div class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$enableCorporateName|escape:'htmlall':'UTF-8'}" value="{$enableCorporate|escape:'htmlall':'UTF-8'}" {if $enableCorporate}checked="checked"{/if} id="corpclients-active">
                                <label for="corpclients-active">{l s='Enable corporate clients support' mod='keycrm'}</label>
                            </div>
                        </div><div style="display:none;" class="keycrm-form__row">
                            <div class="keycrm-form__message-warning">
                                <span>{l s='Activate only in case if you have enabled the option “Corporate customers” in KeyCRM.app' mod='keycrm'}</span>
                            </div>
                        </div>
                    </div>
                    <div id="rcrm_tab_carts_sync">
                        <div class="keycrm-form__title">{l s='Synchronization of buyer carts' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                            <div class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$synchronizeCartsActiveName|escape:'htmlall':'UTF-8'}" value="{$synchronizeCartsActive|escape:'htmlall':'UTF-8'}" {if $synchronizeCartsActive}checked="checked"{/if} id="ac-active">
                                <label for="ac-active">{l s='Create orders for abandoned carts of buyers' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div class="keycrm-form__row">
                            <label class="keycrm-form__label" for="{$synchronizedCartStatusName|escape:'htmlall':'UTF-8'}">{l s='Order status for abandoned carts of buyers' mod='keycrm'}</label>
                            <select placeholder="{l s='Choose status' mod='keycrm'}" class="jq-select" name="{$synchronizedCartStatusName|escape:'htmlall':'UTF-8'}" id="{$synchronizedCartStatusName|escape:'htmlall':'UTF-8'}">
                                {foreach from=$statusesDefaultExport item=cartStatus}
                                    <option value="{$cartStatus.id_option|escape:'htmlall':'UTF-8'}"{if $cartStatus.id_option == $synchronizedCartStatus} selected{/if}>{$cartStatus.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="keycrm-form__row">
                            <label class="keycrm-form__label" for="{$synchronizedCartDelayName|escape:'htmlall':'UTF-8'}">{l s='Upload abandoned carts' mod='keycrm'}</label>
                            <select placeholder="{l s='Choose delay' mod='keycrm'}" class="jq-select" name="{$synchronizedCartDelayName|escape:'htmlall':'UTF-8'}" id="{$synchronizedCartDelayName|escape:'htmlall':'UTF-8'}">
                                {foreach from=$cartsDelays item=delay}
                                    <option value="{$delay.id_option|escape:'htmlall':'UTF-8'}"{if $delay.id_option == $synchronizedCartDelay} selected{/if}>{$delay.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div style="display:none;" id="rcrm_tab_catalog">
                        {assign var="showUpdateButton" value=false}
                        <div class="keycrm-form__title">
                            {$catalogTitleName|escape:'htmlall':'UTF-8'}
                            {if $catalogInfoMultistore|count == 1}
                                {assign var='catalogInfo' value=$catalogInfoMultistore[$catalogInfoMultistore|@key] }
                                <a href="{$url|cat:'/admin/sites/'|escape:'htmlall':'UTF-8'}{if isset($catalogInfo.siteId) and $catalogInfo.siteId}{$catalogInfo.siteId|cat:'/edit#t-catalog'|escape:'htmlall':'UTF-8'}{/if}"
                                   target="_blank"
                                   class="keycrm-form__title_link">{l s='Manage site settings' mod='keycrm'}</a>
                            {/if}
                        </div>
                        {foreach from=$catalogInfoMultistore key=catalogShopId item=catalogInfo}
                        <div class="keycrm-form__title" style="margin-top: 50px;">
                            {if $catalogInfoMultistore|count > 1}
                                <a href="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&rcrmtab=rcrm_tab_catalog&setShopContext=s-{$catalogShopId|escape:'htmlall':'UTF-8'}">{$shopsInfo[$catalogShopId].name|escape:'htmlall':'UTF-8'}</a>
                                <a href="{$url|cat:'/admin/sites/'|escape:'htmlall':'UTF-8'}{if isset($catalogInfo.siteId) and $catalogInfo.siteId}{$catalogInfo.siteId|cat:'/edit#t-catalog'|escape:'htmlall':'UTF-8'}{/if}"
                                   target="_blank"
                                   class="keycrm-form__title_link">{l s='Manage site settings' mod='keycrm'}</a>
                            {/if}
                        </div>
                        {if $catalogInfo and isset($catalogInfo.lastGenerated) and $catalogInfo.lastGenerated}
                            {if  $catalogInfo.isOutdated }
                                <div class="keycrm-alert keycrm-alert-danger">
                                    <div class="keycrm-alert-text">
                                        {$catalogTitleName|escape:'htmlall':'UTF-8'} {l s='is outdated' mod='keycrm'}
                                    </div>
                            {elseif !isset($catalogInfo.isUrlActual) or !$catalogInfo.isUrlActual}
                                    {assign var="showUpdateButton" value=true}
                                <div class="keycrm-alert keycrm-alert-warning">
                                    <div class="keycrm-alert-text">
                                        {l s='URL for Icml catalog file in Prestashop and in %s do not match' mod='keycrm' sprintf=[$systemName]}
                                    </div>
                            {else}
                                <div class="keycrm-alert keycrm-alert-success">
                                    <div class="keycrm-alert-text">
                                        {$catalogTitleName|escape:'htmlall':'UTF-8'} {l s='connected' mod='keycrm'}
                                    </div>
                            {/if}
                                    <div class="keycrm-alert-note">
                                        {$catalogInfo.lastGenerated|date_format:"%Y-%m-%d %H:%M:%S"|escape:'htmlall':'UTF-8'}
                                    </div>
                                </div>
                                <div class="keycrm-form__row">
                                    <div class="keycrm-form__label">
                                    <span style="font-weight: bold; font-size: 1.3em;">
                                    {if $catalogInfo.lastGeneratedDiff.days > 7}
                                        {l s='More than 7 days' mod='keycrm'}
                                    {else}
                                        {if $catalogInfo.lastGeneratedDiff.days > 0}
                                            {$catalogInfo.lastGeneratedDiff.days|escape:'htmlall':'UTF-8'}  {l s='d' mod='keycrm'}.
                                        {/if}
                                        {if $catalogInfo.lastGeneratedDiff.hours > 0}
                                            {$catalogInfo.lastGeneratedDiff.hours|escape:'htmlall':'UTF-8'}  {l s='h' mod='keycrm'}.
                                        {/if}
                                        {$catalogInfo.lastGeneratedDiff.minutes|escape:'htmlall':'UTF-8'} {l s='min' mod='keycrm'}.
                                    {/if}
                                    </span>
                                        {l s='passed since last run' mod='keycrm'}
                                    </div>
                                    {if isset($catalogInfo.productsCount) and isset($catalogInfo.offersCount)}
                                        <div class="keycrm-form__label">
                                            <span style="font-weight: bold; font-size: 1.3em;">
                                                {$catalogInfo.productsCount|escape:'htmlall':'UTF-8'}
                                            </span>
                                            {l s='Products' mod='keycrm'}
                                            <span style="font-weight: bold; font-size: 1.3em;">
                                                {$catalogInfo.offersCount|escape:'htmlall':'UTF-8'}
                                            </span>
                                            {l s='Offers' mod='keycrm'}
                                        </div>
                                    {/if}
                                </div>
                        {else}
                                <div class="keycrm-alert keycrm-alert-warning">
                                    <div class="keycrm-alert-text">
                                        {$catalogTitleName|escape:'htmlall':'UTF-8'} {l s='was not generated yet' mod='keycrm'}
                                    </div>
                                    <div class="keycrm-alert-note">
                                        {l s='Press the below button to generate the %s' mod='keycrm' sprintf=[$catalogTitleName]}
                                    </div>
                                </div>
                        {/if}
                                    {/foreach}
                                <input type="hidden" name="{$runJobName|escape:'htmlall':'UTF-8'}" value="">
                                <div class="keycrm-form__row keycrm-form__row_submit"
                                     style="height: 60px; margin-bottom: 20px; margin-top: 50px; clear:both;">
                                    <button id="update-icml-submit"
                                            class="btn btn_invert btn_warning"
                                            style="outline: none;{if !$showUpdateButton} display: none;{/if}">{l s='Update URL' mod='keycrm'}</button>
                                    <button id="generate-icml-submit"
                                            class="btn btn_invert btn_submit"
                                            style="outline: none;{if $showUpdateButton} display: none;{/if}">{l s='Generate now' mod='keycrm'}</button>
                                </div>
                    </div>
                    <div id="rcrm_tab_delivery_types">
                        <div class="keycrm-form__title">
                            {l s='Delivery' mod='keycrm'}
                            <a href="{$url|cat:'/admin/delivery-types'|escape:'htmlall':'UTF-8'}" target="_blank" class="keycrm-form__title_link">{l s='Manage delivery types' mod='keycrm'}</a>
                        </div>
                        {foreach from=$deliveryTypes item=item}
                            <div class="keycrm-form__row">
                                <label class="keycrm-form__label" for="{$item.name|escape:'htmlall':'UTF-8'}">{l s=$item.label mod='keycrm'}</label>
                                <select placeholder="{l s='Choose delivery' mod='keycrm'}" class="jq-select" name="{$item.name|escape:'htmlall':'UTF-8'}" id="{$item.name|escape:'htmlall':'UTF-8'}">
                                    {foreach from=$item.options.query item=option}
                                        <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($delivery[$item.subname]) && $delivery[$item.subname] == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/foreach}
                    </div>
                    <div id="rcrm_tab_order_statuses">
                        <div class="keycrm-form__title">
                            {l s='Order statuses' mod='keycrm'}
                            <a href="{$url|cat:'/admin/statuses'|escape:'htmlall':'UTF-8'}" target="_blank" class="keycrm-form__title_link">{l s='Manage order statuses' mod='keycrm'}</a>
                        </div>
                        {foreach from=$orderStatuses item=item}
                            <div class="keycrm-form__row">
                                <label class="keycrm-form__label" for="{$item.name|escape:'htmlall':'UTF-8'}">{l s=$item.label mod='keycrm'}</label>
                                <select placeholder="{l s='Choose status' mod='keycrm'}" class="jq-select" name="{$item.name|escape:'htmlall':'UTF-8'}" id="{$item.name|escape:'htmlall':'UTF-8'}">
                                    {foreach from=$item.options.query item=option}
                                        <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($status[$item.subname]) && $status[$item.subname] == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/foreach}
                    </div>
                    <div id="rcrm_tab_payment_types">
                        <div class="keycrm-form__title">
                            {l s='Payment types' mod='keycrm'}
                            <a href="{$url|cat:'/admin/payment-types'|escape:'htmlall':'UTF-8'}" target="_blank" class="keycrm-form__title_link">{l s='Manage payment types' mod='keycrm'}</a>
                        </div>
                        {foreach from=$paymentTypes item=item}
                            <div class="keycrm-form__row">
                                <label class="keycrm-form__label" for="{$item.name|escape:'htmlall':'UTF-8'}">{l s=$item.label mod='keycrm'}</label>
                                <select placeholder="{l s='Choose type' mod='keycrm'}" class="jq-select" name="{$item.name|escape:'htmlall':'UTF-8'}" id="{$item.name|escape:'htmlall':'UTF-8'}">
                                    {foreach from=$item.options.query item=option}
                                        <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($payment[$item.subname]) && $payment[$item.subname] == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/foreach}
                    </div>
                    <div id="rcrm_tab_default_types">
                        <div class="keycrm-form__title">{l s='Default' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                            <label class="keycrm-form__label" for="{$methodsForDefault[0].name|escape:'htmlall':'UTF-8'}">{l s=$methodsForDefault[0].label mod='keycrm'}</label>
                            <select placeholder="{l s='Choose delivery' mod='keycrm'}" class="jq-select" name="{$methodsForDefault[0].name|escape:'htmlall':'UTF-8'}" id="{$methodsForDefault[0].name|escape:'htmlall':'UTF-8'}">
                                {foreach from=$methodsForDefault[0].options.query item=option}
                                    <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($deliveryDefault) && $deliveryDefault == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="keycrm-form__row">
                            <label class="keycrm-form__label" for="{$methodsForDefault[1].name|escape:'htmlall':'UTF-8'}">{l s=$methodsForDefault[1].label mod='keycrm'}</label>
                            <select placeholder="{l s='Choose type' mod='keycrm'}" class="jq-select" name="{$methodsForDefault[1].name|escape:'htmlall':'UTF-8'}" id="{$methodsForDefault[1].name|escape:'htmlall':'UTF-8'}">
                                {foreach from=$methodsForDefault[1].options.query item=option}
                                    <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($paymentDefault) && $paymentDefault == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                        <div class="keycrm-form__row">
                            <label class="keycrm-form__label" for="{$methodsForDefault[2].name|escape:'htmlall':'UTF-8'}">{l s=$methodsForDefault[2].label mod='keycrm'}</label>
                            <select placeholder="{l s='Choose site' mod='keycrm'}" class="jq-select" name="{$methodsForDefault[2].name|escape:'htmlall':'UTF-8'}" id="{$methodsForDefault[2].name|escape:'htmlall':'UTF-8'}">
                                {foreach from=$methodsForDefault[2].options.query item=option}
                                    <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($siteDefault) && $siteDefault == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div style="display:none;"  id="rcrm_tab_stock">
                        <div class="keycrm-form__title">
                            {l s='Stock settings' mod='keycrm'}
                        </div>
                        <div style="display:none;" class="keycrm-form__row">
                            <div class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$enableBalancesReceivingName|escape:'htmlall':'UTF-8'}" value="{$enableBalancesReceiving|escape:'htmlall':'UTF-8'}" {if $enableBalancesReceiving}checked="checked"{/if} id="receivebalances-active">
                                <label for="receivebalances-active">{l s='Receive stocks from KeyCRM.app' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div class="keycrm-form__title" style="margin-top: 40px;">
                            {l s='Out of stock' mod='keycrm'}
                        </div>
                        <div class="keycrm-form__label">
                            {l s='Changing of order status if the product is out of stock and its ordering with zero stock is denied.' mod='keycrm'}
                        </div>
                        {foreach from=$outOfStockStatuses item=item}
                            <div class="keycrm-form__row">
                                <label class="keycrm-form__label" for="{$item.name|escape:'htmlall':'UTF-8'}">{l s=$item.label mod='keycrm'}</label>
                                <select placeholder="{l s='Choose status' mod='keycrm'}" class="jq-select" name="{$item.name|escape:'htmlall':'UTF-8'}" id="{$item.name|escape:'htmlall':'UTF-8'}">
                                    {foreach from=$item.options.query item=option}
                                        <option value="{$option.id_option|escape:'htmlall':'UTF-8'}"{if isset($outOfStockStatus[$item.subname]) && $outOfStockStatus[$item.subname] == $option.id_option} selected{/if}>{$option.name|escape:'htmlall':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/foreach}
                    </div>

                    <div style="display:none;" id="rcrm_tab_daemon_collector">
                        <div class="keycrm-form__title">{l s='Daemon Collector' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                            <div class="keycrm-form__checkbox">
                                <input type="checkbox" name="{$collectorActiveName|escape:'htmlall':'UTF-8'}" value="{$collectorActive|escape:'htmlall':'UTF-8'}" {if $collectorActive}checked="checked"{/if} id="dc-active">
                                <label for="dc-active">{l s='Active' mod='keycrm'}</label>
                            </div>
                        </div>
                        <div class="keycrm-form__row">
                            <input type="text" name="{$collectorKeyName|escape:'htmlall':'UTF-8'}" value="{$collectorKey|escape:'htmlall':'UTF-8'}" class="keycrm-form__area" placeholder="{l s='Site key' mod='keycrm'}">
                        </div>
                    </div>
                    <div class="keycrm-form__row keycrm-form__row_submit" id="main-submit">
                        <input type="submit" value="{l s='Save' mod='keycrm'}" class="btn btn_invert btn_submit">
                    </div>
                </form>

                <div id="rcrm_tab_orders_upload">
                    <div class="keycrm-container--foldable">
                        <div class="keycrm-row--foldable active">
                            <div class="keycrm-form__title keycrm-row__title">{l s='Upload orders' mod='keycrm'}</div>
                            <div class="keycrm-form__row keycrm-row__content">
                                <div class="keycrm-form__label">{l s='Enter order IDs to upload to KeyCRM.app, divided by a comma. You can also specify ranges, like "1-10". It\'s allowed to upload to 10 orders at a time.' mod='keycrm'}</div>

                                <form class="rcrm-form-submit-trigger"
                                      action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm"
                                      method="post"
                                >
                                    <input type="hidden" name="submitkeycrm" value="1"/>

                                    <div class="keycrm-form__row">
                                        <input type="text" name="{$uploadOrders|escape:'htmlall':'UTF-8'}" value=""
                                               class="keycrm-form__area"
                                               placeholder="{l s='Orders IDs' mod='keycrm'}">
                                    </div>
                                    <div class="keycrm-form__row keycrm-form__row_submit">
                                        <button id="upload-orders-submit" class="btn btn_invert btn_submit"
                                                style="outline: none;">{l s='Upload' mod='keycrm'}</button>
                                    </div>
                                </form>
                                <div class="keycrm-form__label">{l s='You can export all the orders and customers from CMS to KeyCRM.app by pressing "Export" button. This process can take a long time, and it\'s required that you keep the tab opened until it\'s done.' mod='keycrm'}</div>

                                <form class="rcrm-form-submit-trigger"
                                      action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm"
                                      method="post"
                                >
                                    <div class="keycrm-form__row">
                                        <div class="keycrm-circle">
                                            <div class="keycrm-circle__title">{l s='Orders' mod='keycrm'}</div>
                                            <input type="text" name="KEYCRM_EXPORT_ORDERS_COUNT" readonly="readonly"
                                                   class="keycrm-circle__content"
                                                   value="{$ordersCount|escape:'htmlall':'UTF-8'}"/>
                                            <input type="hidden" name="KEYCRM_EXPORT_ORDERS_STEP_SIZE"
                                                   value="{$exportOrdersStepSize|escape:'htmlall':'UTF-8'}"/>
                                        </div>
                                        <div class="keycrm-circle">
                                            <div class="keycrm-circle__title">
                                                {l s='Customers' mod='keycrm'}
                                            </div>
                                            <input type="text" readonly="readonly"
                                                   title="{l s='Customers without orders' mod='keycrm'}: {$exportCustomersCount|escape:'htmlall':'UTF-8'}"
                                                   class="keycrm-circle__content"
                                                   value="{$customersCount|escape:'htmlall':'UTF-8'}"/>
                                            <input type="hidden" name="KEYCRM_EXPORT_CUSTOMERS_COUNT"
                                                   value="{$exportCustomersCount|escape:'htmlall':'UTF-8'}"/>
                                            <input type="hidden" name="KEYCRM_EXPORT_CUSTOMERS_STEP_SIZE"
                                                   value="{$exportCustomersStepSize|escape:'htmlall':'UTF-8'}"/>
                                        </div>
                                    </div>
                                    <div class="keycrm-form__row keycrm-form__row_submit"
                                         style="text-align: center; height: 60px; margin-bottom: 20px; clear:both;">
                                        <button id="export-orders-submit" class="btn btn_invert btn_submit"
                                                style="outline: none;">{l s='Export' mod='keycrm'}</button>
                                        <div id="export-orders-progress" class="keycrm-progress keycrm-hidden"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="keycrm-row--foldable" id="keycrm-uploaded-orders-tab">
                            <div class="keycrm-form__title keycrm-row__title">{l s='Uploaded orders' mod='keycrm'}</div>
                            <div class="keycrm-form__row keycrm-row__content">
                                <div class="keycrm-form__label">{l s='In this section you can check the orders export results and manualy upload order to' mod='keycrm'} {$systemName|escape:'htmlall':'UTF-8'}</div>
                                <div class="keycrm-form__row">
                                    <form action="{$keycrmControllerOrders|escape:'htmlall':'UTF-8'}"
                                          id="keycrm-search-orders-form" method="GET">
                                        <input type="text" name="search-orders-value" value=""
                                               class="keycrm-form__area"
                                               placeholder="{l s='Orders IDs' mod='keycrm'}">

                                        <button id="search-orders-submit" class="btn"
                                                style="outline: none;">{l s='Search' mod='keycrm'}</button>

                                        <div class="keycrm-table-filter">
                                            <label for="keycrm-table-filter-status-all"
                                                   class="keycrm-table-filter-btn active">
                                                <span>{l s='All' mod='keycrm'}</span>
                                                <input type="radio" name="search-orders-filter"
                                                       id="keycrm-table-filter-status-all"
                                                       value="0" class="search-orders-filter" checked>
                                            </label>
                                            <label for="keycrm-table-filter-status-ok"
                                                   class="keycrm-table-filter-btn">
                                                <span>{l s='Uploaded' mod='keycrm'}</span>
                                                <input type="radio" name="search-orders-filter"
                                                       id="keycrm-table-filter-status-ok"
                                                       value="1" class="search-orders-filter">
                                            </label>
                                            <label for="keycrm-table-filter-status-fail"
                                                   class="keycrm-table-filter-btn">
                                                <span>{l s='Error' mod='keycrm'}</span>
                                                <input type="radio" name="search-orders-filter"
                                                       id="keycrm-table-filter-status-fail"
                                                       value="2" class="search-orders-filter">
                                            </label>
                                        </div>
                                    </form>
                                </div>
                                <div class="keycrm-form__row keycrm-table-pagination"></div>
                                <div class="keycrm-table-wrapper">
                                    <a href="{$keycrmControllerOrdersUpload|escape:'htmlall':'UTF-8'}"
                                       id="keycrm-controller-orders-upload" class="keycrm-controller-link"></a>
                                    <table class="keycrm-table keycrm-table-sort keycrm-table-center hidden"
                                           id="keycrm-orders-table">
                                        <thead>
                                        <tr>
                                            <th><span>{l s='Date and Time' mod='keycrm'}</span></th>
                                            <th><span>{l s='ID in' mod='keycrm'} Prestashop</span></th>
                                            <th>
                                                <span>{l s='ID in' mod='keycrm'} {$systemName|escape:'htmlall':'UTF-8'}</span>
                                            </th>
                                            <th><span>{l s='Status' mod='keycrm'}</span></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="keycrm-orders-table__date"></td>
                                            <td class="keycrm-orders-table__id-cms">
                                                <a href="{$adminControllerOrders|escape:'htmlall':'UTF-8'}"
                                                   target="_blank"></a>
                                            </td>
                                            <td class="keycrm-orders-table__id-crm">
                                                <a href="{$url|cat:'/orders/'|escape:'htmlall':'UTF-8'}"
                                                   target="_blank"></a>
                                            </td>
                                            <td class="keycrm-orders-table__status">
                                                <p class="keycrm-orders-table__status--success">
                                                    <span style="color: #2e8b57;">&#10004;</span>
                                                    {l s='Uploaded' mod='keycrm'}
                                                </p>
                                                <p class="keycrm-orders-table__status--error">
                                                    <input type="checkbox" class="keycrm-collapsible__input">
                                                    <label class="keycrm-collapsible__title">
                                                        <span style="color: #dd2e44;">&#10060;</span>
                                                        {l s='Error' mod='keycrm'}
                                                        <span class="keycrm-orders-table__error keycrm-collapsible__content"></span>
                                                    </label>
                                                </p>
                                            </td>
                                            <td class="keycrm-table-center keycrm-orders-table__upload">
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                     y="0px" viewBox="0 0 29.978 29.978"
                                                     xml:space="preserve"
                                                     class="keycrm-btn-svg"
                                                >
                                                    <g>
                                                        <path d="M25.462,19.105v6.848H4.515v-6.848H0.489v8.861c0,1.111,0.9,2.012,2.016,2.012h24.967c1.115,0,2.016-0.9,2.016-2.012   v-8.861H25.462z"/>
                                                        <path d="M14.62,18.426l-5.764-6.965c0,0-0.877-0.828,0.074-0.828s3.248,0,3.248,0s0-0.557,0-1.416c0-2.449,0-6.906,0-8.723   c0,0-0.129-0.494,0.615-0.494c0.75,0,4.035,0,4.572,0c0.536,0,0.524,0.416,0.524,0.416c0,1.762,0,6.373,0,8.742   c0,0.768,0,1.266,0,1.266s1.842,0,2.998,0c1.154,0,0.285,0.867,0.285,0.867s-4.904,6.51-5.588,7.193   C15.092,18.979,14.62,18.426,14.62,18.426z"/>
                                                    </g>
                                                </svg>
                                                {l s='Upload' mod='keycrm'}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="display:none;" id="rcrm_tab_consultant">
                    <form class="rcrm-form-submit-trigger"
                          action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&item=consultant"
                          method="post">
                        <input type="hidden" name="submitkeycrm" value="1"/>
                        <div class="keycrm-form__title">{l s='Online consultant' mod='keycrm'}</div>
                        <div class="keycrm-form__row">
                        <textarea name="{$consultantScriptName|escape:'htmlall':'UTF-8'}"
                                  class="keycrm-form__area keycrm-form__area_txt" id="keycrm-txt-area"
                                  placeholder="{l s='Code you need to insert on the web' mod='keycrm'}">
                            {$consultantScript|escape:'htmlall':'UTF-8'}
                        </textarea>
                        </div>
                        <div class="keycrm-form__row keycrm-form__row_submit">
                            <input type="submit" value="{l s='Save' mod='keycrm'}" class="btn btn_invert btn_submit">
                        </div>
                    </form>
                </div>

                <div id="rcrm_tab_advanced">
                    <div class="keycrm-form__title">{l s='Advanced' mod='keycrm'}</div>
                    <div class="keycrm-form__row">
                        <div class="keycrm-form__checkbox">
                            <input form="submitkeycrm-form" type="checkbox"
                                   name="{$debugModeName|escape:'htmlall':'UTF-8'}"
                                   value="{$debugMode|escape:'htmlall':'UTF-8'}"
                                   {if $debugMode}checked="checked"{/if} id="debugmode-active">
                            <label for="debugmode-active"
                                   class="keycrm-form__label">{l s='Debug mode' mod='keycrm'}</label>
                        </div>
                        <div class="keycrm-form__checkbox">
                            <input form="submitkeycrm-form" type="checkbox"
                                   name="{$webJobsName|escape:'htmlall':'UTF-8'}"
                                   value="{$webJobs|escape:'htmlall':'UTF-8'}"
                                   {if $webJobs}checked="checked"{/if} id="webjobs-active">
                            <label for="webjobs-active"
                                   class="keycrm-form__label">{l s='Web Jobs' mod='keycrm'}</label>
                        </div>
                    </div>

                    <div class="keycrm-form__row">
                        <input form="submitkeycrm-form" type="submit" value="{l s='Save' mod='keycrm'}" class="btn btn_invert btn_submit">
                    </div>

                    <div class="keycrm-form__row">
                        <label class="keycrm-form__label">{l s='Job Manager' mod='keycrm'}</label>
                        <div class="keycrm-table-wrapper">
                            <table class="keycrm-table keycrm-table-sort">
                                <thead>
                                <tr>
                                    <th>
                                        <span>{l s='Job name' mod='keycrm'}</span></th>
                                    <th>
                                        <div class="keycrm-table-sort__btn-wrap">
                                            <span class="keycrm-table-sort__asc keycrm-table-sort__btn">&#x25B2</span>
                                            <span class="keycrm-table-sort__desc keycrm-table-sort__btn keycrm-table-sort__initial">&#x25BC</span>
                                        </div>
                                        <span class="keycrm-table-sort__switch">{l s='Last Run' mod='keycrm'}</span>
                                    </th>
                                    <th>
                                        <span>{l s='Status' mod='keycrm'}</span></th>
                                    <th>
                                        <span>{l s='Comment' mod='keycrm'}</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach from=$lastRunDetails key=key item=item}
                                    <tr class="{if $key === $currentJob || $key === $currentJobCli} keycrm-table__row-bold{/if}">
                                        <td>
                                            {if isset($jobsNames[$key]) }
                                                <span title="{$key|escape:'htmlall':'UTF-8'}">{l s=$jobsNames[$key] mod='keycrm'}</span>
                                            {else}
                                                {$key|escape:'htmlall':'UTF-8'}
                                            {/if}
                                        </td>
                                        <td class="keycrm-table-center keycrm-table-no-wrap">{if isset($item['lastRun'])}{$item['lastRun']|date_format:'Y-m-d H:i:s'|escape:'htmlall':'UTF-8'}{/if}</td>
                                        <td class="keycrm-table-center">
                                            {if $key === $currentJob || $key === $currentJobCli}
                                                <span>&#8987;</span>
                                            {else}
                                                {if isset($item['success'])}
                                                    {if $item['success'] === true}
                                                        <span style="color: #2e8b57;">&#10004;</span>
                                                    {else}
                                                        <span style="color: #dd2e44;">&#10060;</span>
                                                    {/if}
                                                {/if}
                                            {/if}
                                        </td>
                                        <td class="keycrm-collapsible">
                                            {if isset($item['error']['message'])}
                                                <input type="checkbox" class="keycrm-collapsible__input"
                                                       id="error_{$key|escape:'htmlall':'UTF-8'}">
                                                <label for="error_{$key|escape:'htmlall':'UTF-8'}"
                                                       class="keycrm-collapsible__title keycrm-error-msg">
                                                    <span class="keycrm-error-msg">{$item['error']['message']|escape:'htmlall':'UTF-8'}</span>
                                                    <p class="keycrm-collapsible__content">
                                                        <b>{l s='StackTrace' mod='keycrm'}
                                                            :</b><br>{$item['error']['trace']|escape:'htmlall':'UTF-8'}
                                                    </p>
                                                </label>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="keycrm-form__row keycrm-form__row_submit">
                        <form class="rcrm-form-submit-trigger"
                              action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&amp;ajax=1"
                              method="post">
                            <input type="submit" id="reset-jobs-submit" class="btn btn_submit"
                                   value="{l s='Reset jobs' mod='keycrm'}"/>
                        </form>
                    </div>

                    <div class="keycrm-form__row">
                        <label class="keycrm-form__label">{l s='Logs' mod='keycrm'}</label>
                        <div class="keycrm-table-wrapper">
                            <table class="keycrm-table keycrm-table-sort">
                                <thead>
                                <tr>
                                    <th><span>{l s='File name' mod='keycrm'}</span></th>
                                    <th>
                                        <div class="keycrm-table-sort__btn-wrap">
                                            <span class="keycrm-table-sort__asc keycrm-table-sort__btn">&#x25B2</span>
                                            <span class="keycrm-table-sort__desc keycrm-table-sort__btn keycrm-table-sort__initial">&#x25BC</span>
                                        </div>
                                        <span class="keycrm-table-sort__switch">{l s='Modified date' mod='keycrm'}</span>
                                    </th>
                                    <th>
                                        <div class="keycrm-table-sort__btn-wrap">
                                            <span class="keycrm-table-sort__asc keycrm-table-sort__btn">&#x25B2</span>
                                            <span class="keycrm-table-sort__desc keycrm-table-sort__btn">&#x25BC</span>
                                        </div>
                                        <span class="keycrm-table-sort__switch">{l s='Size' mod='keycrm'}</span>
                                    </th>
                                    <th><span>{l s='Actions' mod='keycrm'}</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach from=$keycrmLogsInfo key=key item=logItem}
                                    <tr class="keycrm-table__row-top">
                                        <td>{$logItem.name|escape:'htmlall':'UTF-8'}</td>
                                        <td class="keycrm-table-center">{$logItem.modified|escape:'htmlall':'UTF-8'}</td>
                                        <td class="keycrm-table-center">{$logItem.size|escape:'htmlall':'UTF-8'}</td>
                                        <td class="keycrm-table-center">
                                            <form class="rcrm-form-submit-trigger"
                                                  action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&amp;ajax=1"
                                                  method="post">
                                                <input type="hidden" name="submitkeycrm" value="1"/>
                                                <input type="hidden" name="KEYCRM_DOWNLOAD_LOGS" value="1"/>
                                                <input type="hidden" name="KEYCRM_DOWNLOAD_LOGS_NAME"
                                                       value="{$logItem.name|escape:'htmlall':'UTF-8'}"/>
                                                <input type="submit" id="download-log-{$key|escape:'htmlall':'UTF-8'}" style="display: none;"/>
                                                <label for="download-log-{$key|escape:'htmlall':'UTF-8'}"
                                                       style="width: 100%; text-align: center;"
                                                       class="keycrm-btn-svg_wrapper"
                                                       title=" {l s='Download' mod='keycrm'}"
                                                >
                                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                                         y="0px" viewBox="0 0 29.978 29.978"
                                                         xml:space="preserve"
                                                         class="keycrm-btn-svg"
                                                    >
                                                    <g>
                                                        <path d="M25.462,19.105v6.848H4.515v-6.848H0.489v8.861c0,1.111,0.9,2.012,2.016,2.012h24.967c1.115,0,2.016-0.9,2.016-2.012   v-8.861H25.462z"/>
                                                        <path d="M14.62,18.426l-5.764-6.965c0,0-0.877-0.828,0.074-0.828s3.248,0,3.248,0s0-0.557,0-1.416c0-2.449,0-6.906,0-8.723   c0,0-0.129-0.494,0.615-0.494c0.75,0,4.035,0,4.572,0c0.536,0,0.524,0.416,0.524,0.416c0,1.762,0,6.373,0,8.742   c0,0.768,0,1.266,0,1.266s1.842,0,2.998,0c1.154,0,0.285,0.867,0.285,0.867s-4.904,6.51-5.588,7.193   C15.092,18.979,14.62,18.426,14.62,18.426z"/>
                                                    </g>
                                                    </svg>
                                                    {l s='Download' mod='keycrm'}
                                                </label>
                                            </form>
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                        <div class="keycrm-form__row keycrm-form__row_submit">
                            <form class="rcrm-form-submit-trigger"
                                  action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm&amp;ajax=1"
                                  method="post">
                                <input type="hidden" name="submitkeycrm" value="1"/>
                                <input type="hidden" name="KEYCRM_DOWNLOAD_LOGS" value="1"/>
                                <button type="submit" id="download-log-all"  class="btn btn_submit">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                         y="0px" viewBox="0 0 29.978 29.978"
                                         style="width: 20px; fill: #0068FF;" xml:space="preserve">
                                        <g>
                                            <path d="M25.462,19.105v6.848H4.515v-6.848H0.489v8.861c0,1.111,0.9,2.012,2.016,2.012h24.967c1.115,0,2.016-0.9,2.016-2.012   v-8.861H25.462z"/>
                                            <path d="M14.62,18.426l-5.764-6.965c0,0-0.877-0.828,0.074-0.828s3.248,0,3.248,0s0-0.557,0-1.416c0-2.449,0-6.906,0-8.723   c0,0-0.129-0.494,0.615-0.494c0.75,0,4.035,0,4.572,0c0.536,0,0.524,0.416,0.524,0.416c0,1.762,0,6.373,0,8.742   c0,0.768,0,1.266,0,1.266s1.842,0,2.998,0c1.154,0,0.285,0.867,0.285,0.867s-4.904,6.51-5.588,7.193   C15.092,18.979,14.62,18.426,14.62,18.426z"/>
                                        </g>
                                    </svg>
                                    {l s='Download All' mod='keycrm'}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </article>
    </div>

</div>
{*<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>*}
<script>window.jQuery || document.write('<script src="{$assets|escape:'htmlall':'UTF-8'}/js/vendor/jquery-3.4.0.min.js"><\/script>')</script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/vendor/jquery.sumoselect.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-tabs.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-upload.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-orders.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-icml.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-export.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm-advanced.min.js"></script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm.min.js"></script>
