{**
 * MIT License
 *
 * Copyright (c) 2020 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2020 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 *}
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{$assets|escape:'htmlall':'UTF-8'}/css/styles.min.css">
<title>KeyCRM.app</title>
<div class="keycrm keycrm-wrap">
    {include file='./module_messages.tpl'}
    <div class="keycrm-container">
        <div class="keycrm-video">
            <img src="{$assets|escape:'htmlall':'UTF-8'}/img/keycrm.png" width="220" height="220" alt="KeyCRM" />
        </div>
        <h1 class="keycrm-title">KeyCRM.app</h1>
        <div class="keycrm-txt keycrm-descript">
            {l s='KeyCRM.app is a service for online stores that can prevent you from losing orders and increase the income at all stages of the funnel.' mod='keycrm'}
        </div>
        <div class="keycrm-btns">
            <a href="#toggle-form" class="btn btn_max toggle-btn">{l s='I have an account in KeyCRM.app' mod='keycrm'}</a>
            <div class="keycrm-btns__separate">{l s='or' mod='keycrm'}</div>
            <a href="{$registerUrl|escape:'htmlall':'UTF-8'}" target="_blank" class="btn btn_max btn_invert">{l s='Get KeyCRM.app for free' mod='keycrm'}</a>
        </div>
        <div class="keycrm-form toggle-box" id="toggle-form">
            <form action="{$url_post|escape:'htmlall':'UTF-8'}&amp;configure=keycrm" method="post">
                <input type="hidden" name="submitkeycrm" value="1" />
                <div class="keycrm-form__title">{l s='Connection Settings' mod='keycrm'}</div>
                <div class="keycrm-form__row">
                    <input required type="hidden" class="keycrm-form__area" placeholder="{l s='KeyCRM.app URL' mod='keycrm'}" name="{$apiUrl|escape:'htmlall':'UTF-8'}" value="https://openapi.keycrm.app/v1">
                </div>
                <div class="keycrm-form__row">
                    <input required type="text" class="keycrm-form__area" placeholder="{l s='API key' mod='keycrm'}" name="{$apiKey|escape:'htmlall':'UTF-8'  }">
                </div>
                <div class="keycrm-form__row keycrm-form__row_submit">
                    <input type="submit" value="{l s='Save' mod='keycrm'}" class="btn btn_invert btn_submit">
                </div>
            </form>
        </div>
        <div class="keycrm-tabs">
            <div class="keycrm-tabs__head">
                <a href="#descript" class="keycrm-tabs__btn keycrm-tabs__btn_active">{l s='Description' mod='keycrm'}</a>
                <a href="#faq" class="keycrm-tabs__btn">FAQ</a>
                <a href="#contacts" class="keycrm-tabs__btn">{l s='Contacts' mod='keycrm'}</a>
            </div>
            <div class="keycrm-tabs__body">
                <div class="keycrm-tabs__item keycrm-tabs__item_active" id="descript" style="display: block;">
                    <p>
                        {l s='KeyCRM.app is a service for online stores that can prevent you from losing orders and increase the income at all stages of the funnel.' mod='keycrm'}
                    </p>
                    <p>
                        {l s='Stop losing leads:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='LiveChat with active involvement will help you to get more orders from the website' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Chatbots and a single Inbox for Facebook Messengers and WhatsApp prevent you from losing hot leads, who are ready to buy' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Welcome chains warm up your leads and encourage them to make their first purchase' mod='keycrm'}</li>
                    </ul>
                    <p>
                        {l s='Bring the orders to payment:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='Up-sales raise the average bill of your orders automatically' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='The abandoned basket scripts increase the number of paid orders' mod='keycrm'}</li>
                    </ul>
                    <p>
                        {l s='Manage order fulfillment process:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='CRM-system helps to receive orders, distribute them among employees, manage their statuses and fulfill them' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Notifications about the status of the order automatically inform the customer about every step of his order ' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='SalesApp is an application for keycrm outlets that helps you to increase offline sales and builds a customer base in a single system' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Integration with the catalog helps to take into account the balances, prices and location of goods' mod='keycrm'}</li>
                    </ul>
                    <p>
                        {l s='Make your current customers stay with you:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='CDP (Customer Data Platform) combines the data of your customers from different sources and builds a 360° profile' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Segments help to divide your base into small groups to make your communications more relevant.' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Email, SMS, WhatsApp and Facebook messenger newsletters increase the frequency of purchases in your customer base' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Script "Frequently used goods" helps to automatically remind you to replenish stocks' mod='keycrm'}</li>
                    </ul>
                    <p>
                        {l s='Make your customers come back:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='CRM-remarketing helps to launch ads using KeyCRM.app segments' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Abandoned viewing saves the goods that the client looked at the website and offers to pay for them' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Reactivation campaigns make lost customers come back to your store' mod='keycrm'}</li>
                    </ul>
                    <p>
                        {l s='KeyCRM.app increases the effectiveness of all your marketing channels:' mod='keycrm'}
                    </p>
                    <ul class="keycrm-list">
                        <li class="keycrm-list__item">{l s='LiveChat' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Email' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Facebook Messenger' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='SMS' mod='keycrm'}</li>
                        <li class="keycrm-list__item">{l s='Retargeting' mod='keycrm'}</li>
                    </ul>
                </div>
                <div class="keycrm-tabs__item" id="faq">
                    <div class="keycrm-tile">
                        <div class="keycrm-tile__col">
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='Is there a trial of the module?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='The module has a 14-day trial version in which you can work with the help of the KeyCRM.app module.' mod='keycrm'}</div>
                            </div>
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='What is a user?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='A user is the person who will work with the KeyCRM.app module as the representative of your business or your website. Each user can create a personal profile and have their own access to the tool panel.' mod='keycrm'}</div>
                            </div>
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='In what languages is the module available?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='The KeyCRM.app module is available in the following languages:' mod='keycrm'}
                                    <ul class="keycrm-list">
                                        <li class="keycrm-list__item">{l s='Spanish' mod='keycrm'}</li>
                                        <li class="keycrm-list__item">{l s='English' mod='keycrm'}</li>
                                        <li class="keycrm-list__item">{l s='Russian' mod='keycrm'}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="keycrm-tile__col">
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='How long is the trial?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='The duration of the trial version of the KeyCRM.app module is 14 days.' mod='keycrm'}</div>
                            </div>
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='Is it paid per user or is it paid per account?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='Payment is made per user, if another user is added to the KeyCRM.app system, payment by two users would be made. Each user has the right to an account (web-chat and social networks). In case a user needs to work with more than one account, it is necessary to contact the KeyCRM.app team.' mod='keycrm'}</div>
                            </div>
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='How I can pay?' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">
                                    {l s='The methods to make the payment are:' mod='keycrm'}
                                    <ul class="keycrm-list">
                                        <li class="keycrm-list__item">{l s='Wire transfer' mod='keycrm'}</li>
                                        <li class="keycrm-list__item">{l s='Credit card' mod='keycrm'}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="keycrm-tabs__item" id="contacts">
                    <div class="keycrm-tile">
                        <div class="keycrm-tile__col">
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__title">{l s='Our contacts' mod='keycrm'}</div>
                                <div class="keycrm-tile__descript">{l s='Write us in case of questions or doubts' mod='keycrm'}</div>
                            </div>
                        </div>
                        <div class="keycrm-tile__col keycrm-tile__col_contacts">
                            <div class="keycrm-tile__item">
                                <div class="keycrm-tile__row">
                                    <a href="mailto:{$supportEmail|escape:'htmlall':'UTF-8'}" class="keycrm-tile__link">{$supportEmail|escape:'htmlall':'UTF-8'}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="keycrm-popup-wrap js-popup-close">
    <div class="keycrm-popup" id="video-popup">
        <div class="keycrm-popup__close js-popup-close"></div>
        <div id="player"></div>
    </div>
</div>
{*<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>*}
{*<script>window.jQuery || document.write('<script src="{$assets}/js/vendor/jquery-3.4.0.min.js"><\/script>')</script>*}
<script>window.RCRMPROMO="{$promoVideoUrl|escape:'htmlall':'UTF-8'}";</script>
<script src="{$assets|escape:'htmlall':'UTF-8'}/js/keycrm.min.js"></script>
