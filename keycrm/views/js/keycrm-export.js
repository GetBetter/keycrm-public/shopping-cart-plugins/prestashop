/**
 * MIT License
 *
 * Copyright (c) 2020 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2020 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */
$(function () {
  function KeycrmExportForm() {
    this.form = $("input[name=KEYCRM_EXPORT_ORDERS_COUNT]")
      .closest("form")
      .get(0);

    if (typeof this.form === "undefined") {
      return false;
    }

    this.isDone = false;
    this.ordersCount = parseInt(
      $(this.form).find('input[name="KEYCRM_EXPORT_ORDERS_COUNT"]').val()
    );
    this.customersCount = parseInt(
      $(this.form).find('input[name="KEYCRM_EXPORT_CUSTOMERS_COUNT"]').val()
    );
    this.ordersStepSize = parseInt(
      $(this.form).find('input[name="KEYCRM_EXPORT_ORDERS_STEP_SIZE"]').val()
    );
    this.customersStepSize = parseInt(
      $(this.form).find('input[name="KEYCRM_EXPORT_CUSTOMERS_STEP_SIZE"]').val()
    );
    this.ordersStep = 0;
    this.customersStep = 0;

    this.submitButton = $(this.form)
      .find('button[id="export-orders-submit"]')
      .get(0);
    this.progressBar = $(this.form)
      .find('div[id="export-orders-progress"]')
      .get(0);

    this.submitAction = this.submitAction.bind(this);
    this.exportAction = this.exportAction.bind(this);
    this.exportDone = this.exportDone.bind(this);
    this.initializeProgressBar = this.initializeProgressBar.bind(this);
    this.updateProgressBar = this.updateProgressBar.bind(this);

    $(this.submitButton).click(this.submitAction);
  }

  KeycrmExportForm.prototype.submitAction = function (event) {
    event.preventDefault();

    this.initializeProgressBar();
    this.exportAction();
  };

  KeycrmExportForm.prototype.exportAction = function () {
    let data = {
      submitkeycrm: 1,
      ajax: 1,
    };
    if (this.ordersStep * this.ordersStepSize < this.ordersCount) {
      this.ordersStep++;
      data.KEYCRM_EXPORT_ORDERS_STEP = this.ordersStep;
    } else {
      if (this.customersStep * this.customersStepSize < this.customersCount) {
        this.customersStep++;
        data.KEYCRM_EXPORT_CUSTOMERS_STEP = this.customersStep;
      } else {
        data.KEYCRM_UPDATE_SINCE_ID = 1;
        this.isDone = true;
      }
    }

    let _this = this;

    $.ajax({
      url: this.form.action,
      method: this.form.method,
      timeout: 0,
      data: data,
    }).done(function (response) {
      if (_this.isDone) {
        return _this.exportDone();
      }

      _this.updateProgressBar();
      _this.exportAction();
    });
  };

  KeycrmExportForm.prototype.initializeProgressBar = function () {
    $(this.submitButton).addClass("keycrm-hidden");
    $(this.progressBar)
      .removeClass("keycrm-hidden")
      .append($("<div/>", { class: "keycrm-progress__loader", text: "0%" }));

    window.addEventListener("beforeunload", this.confirmLeave);
  };

  KeycrmExportForm.prototype.updateProgressBar = function () {
    let processedOrders = this.ordersStep * this.ordersStepSize;
    if (processedOrders > this.ordersCount) {
      processedOrders = this.ordersCount;
    }

    let processedCustomers = this.customersStep * this.customersStepSize;
    if (processedCustomers > this.customersCount) {
      processedCustomers = this.customersCount;
    }

    const processed = processedOrders + processedCustomers;
    const total = this.ordersCount + this.customersCount;
    const percents = Math.round((100 * processed) / total);

    $(this.progressBar)
      .find(".keycrm-progress__loader")
      .text(percents + "%");
    $(this.progressBar)
      .find(".keycrm-progress__loader")
      .css("width", percents + "%");
    $(this.progressBar)
      .find(".keycrm-progress__loader")
      .attr("title", processed + "/" + total);
  };

  KeycrmExportForm.prototype.confirmLeave = function (event) {
    event.preventDefault();
    event.returnValue = "Export process has been started";
  };

  KeycrmExportForm.prototype.exportDone = function () {
    window.removeEventListener("beforeunload", this.confirmLeave);
    alert("Export is done");
  };

  window.KeycrmExportForm = KeycrmExportForm;
});
