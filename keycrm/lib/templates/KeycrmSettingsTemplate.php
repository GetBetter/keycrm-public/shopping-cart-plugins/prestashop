<?php
/**
 * MIT License
 *
 * Copyright (c) 2021 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2021 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

class KeycrmSettingsTemplate extends KeycrmAbstractTemplate
{
    protected $settings;
    protected $settingsNames;

    /**
     * KeycrmSettingsTemplate constructor.
     *
     * @param \Module $module
     * @param $smarty
     * @param $assets
     * @param $settings
     * @param $settingsNames
     */
    public function __construct(Module $module, $smarty, $assets, $settings, $settingsNames)
    {
        parent::__construct($module, $smarty, $assets);

        $this->settings = $settings;
        $this->settingsNames = $settingsNames;
    }

    /**
     * Build params for template (Загрузка справочников для вывода в админку)
     *
     * @return mixed
     */
    protected function getParams()
    {
        $params = [];

        if ($this->module->api) {
            $params['statusesDefaultExport'] = $this->module->reference->getStatuseDefaultExport();
            $params['deliveryTypes'] = $this->module->reference->getDeliveryTypes();
//            $params['crmShops'] = $this->module->reference->getSite();
            $params['orderStatuses'] = $this->module->reference->getStatuses();
            $params['outOfStockStatuses'] = $this->module->reference->getOutOfStockStatuses(
                [
                    'out_of_stock_paid' => $this->module->translate('If order paid'),
                    'out_of_stock_not_paid' => $this->module->translate('If order not paid'),
                ]
            );
            $params['paymentTypes'] = $this->module->reference->getPaymentTypes();
            $params['methodsForDefault'] = $this->module->reference->getPaymentAndDeliveryForDefault(
                [
                    $this->module->translate('Delivery method'),
                    $this->module->translate('Payment type'),
                    $this->module->translate('Crm Site'),
                ]
            );
            $params['ordersCount'] = KeycrmExport::getOrdersCount();
            $params['customersCount'] = KeycrmExport::getCustomersCount();
            $params['exportCustomersCount'] = KeycrmExport::getCustomersCount(false);
            $params['exportOrdersStepSize'] = KeycrmExport::KEYCRM_EXPORT_ORDERS_STEP_SIZE_WEB;
            $params['exportCustomersStepSize'] = KeycrmExport::KEYCRM_EXPORT_CUSTOMERS_STEP_SIZE_WEB;
            $params['lastRunDetails'] = KeycrmJobManager::getLastRunDetails(true);
            $params['currentJob'] = Configuration::get(KeycrmJobManager::CURRENT_TASK);
            $params['currentJobCli'] = Configuration::get(KeycrmCli::CURRENT_TASK_CLI);
            $params['keycrmLogsInfo'] = KeycrmLogger::getLogFilesInfo();
            $params['catalogInfoMultistore'] = KeycrmCatalogHelper::getIcmlFileInfoMultistore();
            $params['shopsInfo'] = KeycrmContextSwitcher::getShops();
            $params['errorTabs'] = $this->module->validateStoredSettings();

            $params['keycrmControllerOrders'] = KeycrmTools::getAdminControllerUrl(
                KeycrmOrdersController::class
            );
            $params['keycrmControllerOrdersUpload'] = KeycrmTools::getAdminControllerUrl(
                KeycrmOrdersUploadController::class
            );
            $params['adminControllerOrders'] = KeycrmTools::getAdminControllerUrl(
                AdminOrdersController::class
            );
        }

        return $params;
    }

    protected function buildParams()
    {
        $this->data = array_merge(
            [
                'assets' => $this->assets,
                'cartsDelays' => $this->module->getSynchronizedCartsTimeSelect(),
            ],
            $this->getParams(),
            $this->settingsNames,
            $this->settings
        );
    }

    /**
     * Set template data
     */
    protected function setTemplate()
    {
        $this->template = 'settings.tpl';
    }
}
