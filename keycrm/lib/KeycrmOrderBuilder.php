<?php

use function getIdProductAttributesByIdAttributes as getIdProductAttributesByIdAttributesAlias;

/**
 * MIT License
 *
 * Copyright (c) 2021 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2021 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

class KeycrmOrderBuilder
{
    /**
     * @var \KeycrmApiClientV5
     */
    protected $api;

    /**
     * @var int
     */
    protected $default_lang;

    /**
     * @var Order|OrderCore|null
     */
    protected $cmsOrder;

    /**
     * @var Cart|CartCore|null
     */
    protected $cmsCart;

    /**
     * @var Customer|CustomerCore|null
     */
    protected $cmsCustomer;

    /**
     * @var Address|\AddressCore
     */
    protected $invoiceAddress;

    /**
     * @var Address|\AddressCore
     */
    protected $deliveryAddress;

    /**
     * @var array|null
     */
    protected $createdCustomer;

    /**
     * @var array|null
     */
    protected $corporateCompanyExtractCache;

    /**
     * @var string
     */
    protected $apiSite;

    /**
     * @return KeycrmOrderBuilder
     */
    public function defaultLangFromConfiguration()
    {
        $this->default_lang = (int) Configuration::get('PS_LANG_DEFAULT');

        return $this;
    }

    /**
     * @param mixed $default_lang
     *
     * @return KeycrmOrderBuilder
     */
    public function setDefaultLang($default_lang)
    {
        $this->default_lang = $default_lang;

        return $this;
    }

    /**
     * @param mixed $api
     *
     * @return KeycrmOrderBuilder
     */
    public function setApi($api)
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @param Order|OrderCore $cmsOrder
     *
     * @return KeycrmOrderBuilder
     */
    public function setCmsOrder($cmsOrder)
    {
        $this->cmsOrder = $cmsOrder;

        if ($cmsOrder instanceof Order) {
            if (null === $this->cmsCustomer) {
                $this->cmsCustomer = $cmsOrder->getCustomer();
            }

            if (null === $this->invoiceAddress) {
                $this->invoiceAddress = new Address($cmsOrder->id_address_invoice);
            }

            if (null === $this->deliveryAddress) {
                $this->deliveryAddress = new Address($cmsOrder->id_address_delivery);
            }
        }

        return $this;
    }

    /**
     * @param Cart|CartCore $cmsCart
     *
     * @return KeycrmOrderBuilder
     */
    public function setCmsCart($cmsCart)
    {
        $this->cmsCart = $cmsCart;

        if ($cmsCart instanceof Cart) {
            if (null === $this->cmsCustomer && !empty($cmsCart->id_customer)) {
                $this->cmsCustomer = new Customer($cmsCart->id_customer);
            }

            if (null === $this->invoiceAddress && !empty($cmsCart->id_address_invoice)) {
                $this->invoiceAddress = new Address($cmsCart->id_address_invoice);
            }

            if (null === $this->deliveryAddress && !empty($cmsCart->id_address_delivery)) {
                $this->deliveryAddress = new Address($cmsCart->id_address_delivery);
            }
        }

        return $this;
    }

    /**
     * @param mixed $cmsCustomer
     *
     * @return KeycrmOrderBuilder
     */
    public function setCmsCustomer($cmsCustomer)
    {
        $this->cmsCustomer = $cmsCustomer;

        return $this;
    }

    /**
     * getApiSite
     *
     * @return string|null
     */
    protected function getApiSite()
    {
        $test = KeyCRM::SITE_DEFAULT;
        $siteDefault = json_decode(Configuration::get('KEYCRM_API_SITE_DEFAULT'), true);

        return $this->apiSite;
    }

    /**
     * Clear object
     *
     * @return mixed
     */
    public function reset()
    {
        $this->cmsOrder = null;
        $this->cmsCart = null;
        $this->cmsCustomer = null;
        $this->invoiceAddress = null;
        $this->deliveryAddress = null;
        $this->createdCustomer = null;
        $this->corporateCompanyExtractCache = null;

        return $this;
    }

    /**
     * Returns order with prepared customer data. Customer is created if it's not exist, shouldn't be called to just
     * build order.
     *
     * @param bool $dataFromCart
     *
     * @return array
     */
    public function buildOrderWithPreparedCustomer($dataFromCart = false)
    {
//        if (KeycrmTools::isCorporateEnabled() && KeycrmTools::isOrderCorporate($this->cmsOrder)) {
//            return $this->buildCorporateOrder($dataFromCart);
//        }

        return $this->buildRegularOrder($dataFromCart);
    }

    /**
     * Creates customer if it's not present
     *
     * @return array|bool
     */
    private function createCustomerIfNotExist()
    {
        $this->validateCmsCustomer();

        $customer = $this->findRegularCustomer();

        if (empty($customer)) {
            $crmCustomer = static::buildCrmCustomer($this->cmsCustomer, $this->buildRegularAddress());
            $createResponse = $this->api->customersCreate($crmCustomer);

            if (!$createResponse || !$createResponse->isSuccessful()) {
                $this->createdCustomer = [];

                return false;
            }

            $this->createdCustomer = $this->findRegularCustomer();
            $customer = $this->createdCustomer;
        } else {
            $crmCustomer = KeycrmTools::mergeCustomerAddress($customer, $this->buildRegularAddress());
            if (!KeycrmTools::isEqualCustomerAddress($customer, $crmCustomer)) {
                if (isset($crmCustomer['tags'])) {
                    unset($crmCustomer['tags']);
                }

                $response = $this->api->customersEdit($crmCustomer);

                if ($response instanceof KeycrmApiResponse && $response->isSuccessful()) {
                    $customer = $crmCustomer;
                }
            }
        }

        return isset($customer['id']) ? $customer : false;
    }

    private function buildRegularOrder($dataFromCart = false)
    {
//        $customer = $this->createCustomerIfNotExist();
        $order = static::buildCrmOrder(
            $this->cmsOrder,
            $this->cmsCustomer,
            $this->cmsCart,
            false,
            false,
            $dataFromCart,
            '',
            '',
            isset($customer['id']) ? $customer['id'] : ''
        );

        return KeycrmTools::clearArray($order);
    }

    /**
     * Build regular customer address
     *
     * @return array
     */
    private function buildRegularAddress()
    {
        $addressBuilder = new KeycrmAddressBuilder();

        return $addressBuilder
            ->setAddress($this->invoiceAddress)
            ->build()
            ->getDataArray()
        ;
    }

    /**
     * Builds address array for corporate customer, returns empty array in case of failure.
     *
     * @param bool $isMain
     *
     * @return array
     */
    private function buildCorporateAddress($isMain = true)
    {
        if (empty($this->invoiceAddress) || empty($this->invoiceAddress->id)) {
            return [];
        }

        $addressBuilder = new KeycrmAddressBuilder();

        return $addressBuilder
            ->setMode(KeycrmAddressBuilder::MODE_CORPORATE_CUSTOMER)
            ->setAddress($this->invoiceAddress)
            ->setIsMain($isMain)
            ->setWithExternalId(true)
            ->build()
            ->getDataArray()
        ;
    }

    /**
     * Builds company for corporate customer
     *
     * @param int $addressId
     *
     * @return array
     */
    private function buildCorporateCompany($addressId = 0)
    {
        $companyName = '';
        $vat = '';

        if (!empty($this->invoiceAddress)) {
            if (empty($this->invoiceAddress->company)) {
                $companyName = 'Main Company';
            } else {
                $companyName = $this->invoiceAddress->company;
            }

            if (!empty($this->invoiceAddress->vat_number)) {
                $vat = $this->invoiceAddress->vat_number;
            }
        }

        $company = [
            'isMain' => true,
            'name' => $companyName,
        ];

        if (!empty($addressId)) {
            $company['address'] = [
                'id' => $addressId,
            ];
        }

        if (!empty($vat)) {
            $company['contragent']['INN'] = $vat;
        }

        return KeycrmTools::clearArray($company);
    }

    /**
     * Creates new corporate customer from data, returns false in case of error
     *
     * @return array|bool
     */
    private function createCorporateIfNotExist()
    {
        $corporateWasFound = true;
        $this->validateCmsCustomerInDb();

        $customer = $this->createCustomerIfNotExist();

        if (!$customer) {
            KeycrmLogger::writeCaller(__METHOD__, 'Cannot proceed because customer is empty!');

            return false;
        }

        $crmCorporate = $this->findCorporateCustomerByContactAndCompany(
            $customer['id'],
            $this->invoiceAddress->company
        );

        if (empty($crmCorporate)) {
            $crmCorporate = $this->findCorporateCustomerByCompany($this->invoiceAddress->company);
        }

        if (empty($crmCorporate)) {
            $crmCorporate = $this->createCorporateCustomer($customer['externalId']);
            $corporateWasFound = false;
        } elseif (isset($crmCorporate['id'])) {
            $this->appendAdditionalAddressToCorporate($crmCorporate['id']);
        }

        if ($corporateWasFound) {
            $contactList = $this->api->customersCorporateContacts(
                $crmCorporate['id'],
                ['contactIds' => [$customer['id']]],
                null,
                null,
                'id',
                $this->getApiSite()
            );

            if (!$contactList->offsetExists('contacts')) {
                return $crmCorporate;
            }

            if (0 == count($contactList['contacts'])) {
                $contactData = [
                    'isMain' => false,
                    'customer' => [
                        'id' => $customer['id'],
                        'site' => $this->getApiSite(),
                    ],
                ];

                $crmCorporateCompany = $this->extractCorporateCompanyCached(
                    $crmCorporate['id'],
                    $this->invoiceAddress->company
                );

                if (!empty($crmCorporateCompany) && isset($crmCorporateCompany['id'])) {
                    $contactData['companies'] = [[
                        'company' => ['id' => $crmCorporateCompany['id']],
                    ]];
                }

                $this->api->customersCorporateContactsCreate(
                    $crmCorporate['id'],
                    $contactData,
                    'id',
                    $this->getApiSite()
                );
            }
        }

        return $crmCorporate;
    }

    /**
     * createCorporateCustomer
     *
     * @param string $contactPersonExternalId
     *
     * @return bool|array|\KeycrmApiResponse
     */
    private function createCorporateCustomer($contactPersonExternalId)
    {
        $customerCorporate = static::buildCrmCustomerCorporate(
            $this->cmsCustomer,
            $this->invoiceAddress->company,
            $contactPersonExternalId,
            false,
            false,
            $this->getApiSite()
        );
        $crmCorporate = $this->api->customersCorporateCreate($customerCorporate, $this->getApiSite());

        if (!$crmCorporate || !$crmCorporate->isSuccessful()) {
            return false;
        }

        $address = $this->buildCorporateAddress();
        $createResponse = $this->api->customersCorporateAddressesCreate(
            $crmCorporate['id'],
            $address,
            'id',
            $this->getApiSite()
        );

        if ($createResponse && $createResponse->isSuccessful()) {
            $company = $this->buildCorporateCompany($createResponse['id']);
            $this->api->customersCorporateCompaniesCreate(
                $crmCorporate['id'],
                $company,
                'id',
                $this->getApiSite()
            );
        }

        $crmCorporate = $this->api->customersCorporateGet($crmCorporate['id'], 'id', $this->getApiSite());

        if ($crmCorporate
            && $crmCorporate->isSuccessful()
            && $crmCorporate->offsetExists('customerCorporate')
        ) {
            return $crmCorporate['customerCorporate'];
        }

        return $crmCorporate;
    }

    /**
     * Append new address to corporate customer if new address is not present in corporate customer.
     *
     * @param string|int $corporateId
     */
    private function appendAdditionalAddressToCorporate($corporateId)
    {
        $request = new KeycrmApiPaginatedRequest();
        $address = $this->buildCorporateAddress(false);
        $addresses = $request
            ->setApi($this->api)
            ->setMethod('customersCorporateAddresses')
            ->setParams([
                $corporateId,
                [],
                '{{page}}',
                '{{limit}}',
                'id',
                $this->getApiSite(),
            ])
            ->setDataKey('addresses')
            ->execute()
            ->getData()
        ;

        foreach ($addresses as $addressInCrm) {
            if (!empty($addressInCrm['externalId']) && $addressInCrm['externalId'] == $this->invoiceAddress->id) {
                $this->api->customersCorporateAddressesEdit(
                    $corporateId,
                    $addressInCrm['externalId'],
                    $address,
                    'id',
                    'externalId',
                    $this->getApiSite()
                );

                return;
            }
        }

        $this->api->customersCorporateAddressesCreate(
            $corporateId,
            $address,
            'id',
            $this->getApiSite()
        );
    }

    /**
     * Find self::cmsCustomer in keyCRM by id or by email
     *
     * @return array|mixed
     */
    private function findRegularCustomer()
    {
        $this->validateCmsCustomer();

        if (empty($this->cmsCustomer->id) || $this->cmsCustomer->is_guest) {
            if (!empty($this->cmsCustomer->email)) {
                $customers = $this->api->customersList(['email' => $this->cmsCustomer->email]);

                if ($customers
                    && $customers->isSuccessful()
                    && $customers->offsetExists('customers')
                    && !empty($customers['customers'])
                ) {
                    $customers = $customers['customers'];

                    return reset($customers);
                }
            }
        } else {
            $customer = $this->api->customersGet($this->cmsCustomer->id);

            if ($customer && $customer->isSuccessful() && $customer->offsetExists('customer')) {
                return $customer['customer'];
            }
        }

        return [];
    }

    /**
     * Finds all corporate customers with specified contact id and filters them by provided main company name
     *
     * @param $contactId
     * @param $companyName
     *
     * @return array
     */
    private function findCorporateCustomerByContactAndCompany($contactId, $companyName)
    {
        $crmCorporate = $this->api->customersCorporateList([
            'contactIds' => [$contactId],
            'companyName' => $companyName,
        ]);

        if ($crmCorporate instanceof KeycrmApiResponse
            && $crmCorporate->isSuccessful()
            && $crmCorporate->offsetExists('customersCorporate')
            && 0 < count($crmCorporate['customersCorporate'])
        ) {
            $crmCorporate = $crmCorporate['customersCorporate'];

            return reset($crmCorporate);
        }

        return [];
    }

    /**
     * Find corporate customer by company name
     *
     * @param $companyName
     *
     * @return array
     */
    private function findCorporateCustomerByCompany($companyName)
    {
        $crmCorporate = $this->api->customersCorporateList([
            'companyName' => $companyName,
        ]);

        if ($crmCorporate instanceof KeycrmApiResponse
            && $crmCorporate->isSuccessful()
            && $crmCorporate->offsetExists('customersCorporate')
            && 0 < count($crmCorporate['customersCorporate'])
        ) {
            $crmCorporate = $crmCorporate['customersCorporate'];

            return reset($crmCorporate);
        }

        return [];
    }

    /**
     * Get corporate companies, extract company data by provided identifiers
     *
     * @param int|string $corporateCrmId
     * @param string $companyName
     * @param string $by
     *
     * @return array
     */
    private function extractCorporateCompany($corporateCrmId, $companyName, $by = 'id')
    {
        $companiesResponse = $this->api->customersCorporateCompanies(
            $corporateCrmId,
            [],
            null,
            null,
            $by
        );

        if ($companiesResponse instanceof KeycrmApiResponse
            && $companiesResponse->isSuccessful()
            && $companiesResponse->offsetExists('companies')
            && 0 < count($companiesResponse['companies'])
        ) {
            $company = array_reduce(
                $companiesResponse['companies'],
                function ($carry, $item) use ($companyName) {
                    if (is_array($item) && isset($item['name']) && $item['name'] == $companyName) {
                        $carry = $item;
                    }

                    return $carry;
                }
            );

            if (is_array($company)) {
                return $company;
            }
        }

        return [];
    }

    /**
     * extractCorporateCompany with cache
     *
     * @param int|string $corporateCrmId
     * @param string $companyName
     * @param string $by
     *
     * @return array
     */
    private function extractCorporateCompanyCached($corporateCrmId, $companyName, $by = 'id')
    {
        $cachedItemId = sprintf('%s:%s', (string) $corporateCrmId, $companyName);

        if (!is_array($this->corporateCompanyExtractCache)) {
            $this->corporateCompanyExtractCache = [];
        }

        if (!isset($this->corporateCompanyExtractCache[$cachedItemId])) {
            $this->corporateCompanyExtractCache[$cachedItemId] = $this->extractCorporateCompany(
                $corporateCrmId,
                $companyName,
                $by
            );
        }

        return $this->corporateCompanyExtractCache[$cachedItemId];
    }

    /**
     * Throws exception if cmsCustomer is not set
     *
     * @throws \InvalidArgumentException
     */
    private function validateCmsCustomer()
    {
        if (null === $this->cmsCustomer) {
            throw new \InvalidArgumentException('KeycrmOrderBuilder::cmsCustomer must be set');
        }
    }

    /**
     * Throws exception if cmsCustomer is not set or it's not present in DB yet
     *
     * @throws \InvalidArgumentException
     */
    private function validateCmsCustomerInDb()
    {
        $this->validateCmsCustomer();

        if (empty($this->cmsCustomer->id)) {
            throw new \InvalidArgumentException('KeycrmOrderBuilder::cmsCustomer must be stored in DB');
        }
    }

    private function buildCorporateOrder($dataFromCart = false)
    {
        $customer = $this->createCorporateIfNotExist();
        $contactPersonId = '';
        $contactPersonExternalId = '';

        if (empty($customer)) {
            return [];
        }

        if (empty($this->cmsCustomer->id)) {
            $contacts = $this->api->customersList(['email' => $this->cmsCustomer->email]);

            if ($contacts
                && $contacts->isSuccessful()
                && $contacts->offsetExists('customers')
                && !empty($contacts['customers'])
            ) {
                $contacts = $contacts['customers'];
                $contactPerson = reset($contacts);

                if (isset($contactPerson['id'])) {
                    $contactPersonId = $contactPerson['id'];
                }
            }
        } else {
            $contacts = $this->api->customersCorporateContacts(
                $customer['id'],
                ['contactExternalIds' => [$this->cmsCustomer->id]],
                null,
                null,
                'id',
                $this->getApiSite()
            );

            if ($contacts
                && $contacts->isSuccessful()
                && $contacts->offsetExists('contacts')
                && 1 == count($contacts['contacts'])
            ) {
                $contactPersonExternalId = $this->cmsCustomer->id;
            }
        }

        return static::buildCrmOrder(
            $this->cmsOrder,
            $this->cmsCustomer,
            $this->cmsCart,
            false,
            false,
            $dataFromCart,
            $contactPersonId,
            $contactPersonExternalId,
            $customer['id'],
            $this->getApiSite()
        );
    }

    /**
     * Build array with order data for keyCRM from PrestaShop order data
     *
     * @param Order|\OrderCore $order PrestaShop Order
     * @param Customer|\CustomerCore|null $customer PrestaShop Customer
     * @param Cart|\CartCore|null $orderCart Cart for provided order. Optional
     * @param bool $isStatusExport Use status for export
     * @param bool $preferCustomerAddress Use customer address even if delivery address is
     *                                    provided
     * @param bool $dataFromCart Prefer data from cart
     * @param string $contactPersonId Contact person id to append
     * @param string $contactPersonExternalId contact person externalId to append
     * @param string $customerId Customer id
     * @param string $site Site code (for customer only)
     *
     * @return array keyCRM order data
     *
     * @todo Refactor into OrderBuilder (current order builder should be divided into several independent builders).
     */
    public static function buildCrmOrder(
        $order,
        $customer = null,
        $orderCart = null,
        $isStatusExport = false,
        $preferCustomerAddress = false,
        $dataFromCart = false,
        $contactPersonId = '',
        $contactPersonExternalId = '',
        $customerId = '',
        $site = ''
    ) {
        $CRM_Settings =  [
            'delivery' => json_decode(Configuration::get(KeyCRM::DELIVERY), true),
            'status' => json_decode(Configuration::get(KeyCRM::STATUS), true),
            'outOfStockStatus' => json_decode(Configuration::get(KeyCRM::OUT_OF_STOCK_STATUS), true),
            'payment' => json_decode(Configuration::get(KeyCRM::PAYMENT), true),
            'deliveryDefault' => json_decode(Configuration::get(KeyCRM::DELIVERY_DEFAULT), true),
            'paymentDefault' => json_decode(Configuration::get(KeyCRM::PAYMENT_DEFAULT), true),
            'siteDefault' => json_decode(Configuration::get(KeyCRM::SITE_DEFAULT), true),
            'statusExport' => (string) (Configuration::get(KeyCRM::STATUS_EXPORT)),
            'collectorActive' => (Configuration::get(KeyCRM::COLLECTOR_ACTIVE)),
            'collectorKey' => (string) (Configuration::get(KeyCRM::COLLECTOR_KEY)),
            'clientId' => Configuration::get(KeyCRM::CLIENT_ID),
            'synchronizeCartsActive' => (Configuration::get(KeyCRM::SYNC_CARTS_ACTIVE)),
            'synchronizedCartStatus' => (string) (Configuration::get(KeyCRM::SYNC_CARTS_STATUS)),
            'consultantScript' => (string) (Configuration::get(KeyCRM::CONSULTANT_SCRIPT)),
            'enableCorporate' => (bool) (Configuration::get(KeyCRM::ENABLE_CORPORATE_CLIENTS)),
            'enableHistoryUploads' => (bool) (Configuration::get(KeyCRM::ENABLE_HISTORY_UPLOADS)),
            'enableBalancesReceiving' => (bool) (Configuration::get(KeyCRM::ENABLE_BALANCES_RECEIVING)),
            'enableOrderNumberSending' => (bool) (Configuration::get(KeyCRM::ENABLE_ORDER_NUMBER_SENDING)),
            'enableOrderNumberReceiving' => (bool) (Configuration::get(KeyCRM::ENABLE_ORDER_NUMBER_RECEIVING)),
        ];
        $statusExport = $CRM_Settings['statusExport'];
        $delivery = $CRM_Settings['delivery'];
        $payment = $CRM_Settings['payment'];
        $status = $CRM_Settings['status'];
        $sendOrderNumber = $CRM_Settings['enableOrderNumberSending'];
        $orderNumber = $sendOrderNumber ? $order->id .'-' . $order->reference : $order->id;

        if (false === Module::getInstanceByName('advancedcheckout')) {
            $paymentType = $order->module;
        } else {
            $paymentType = $order->payment;
        }

        if (0 == $order->current_state) {
            $order_status = $statusExport;

            if (!$isStatusExport) {
                $order_status =
                    array_key_exists($order->current_state, $status)
                        ? $status[$order->current_state] : 'new';
            }
        } else {
            $order_status = array_key_exists($order->current_state, $status)
                ? $status[$order->current_state]
                : $statusExport;
        }

        $cart = $orderCart;

        if (null === $cart) {
            $cart = new Cart($order->getCartIdStatic($order->id));
        }

        if (null === $customer) {
            $customer = new Customer($order->id_customer);
        }


        $crmOrder = array_filter([
            'source_id' => (int)$CRM_Settings['siteDefault'],
            'source_uuid' => $orderNumber,
            'ordered_at' => KeycrmTools::verifyDate($order->date_add, 'Y-m-d H:i:s')
                ? $order->date_add : date('Y-m-d H:i:s'),
//            'status' => $order_status,
        ]);

        $addressCollection = $cart->getAddressCollection();
        $addressDelivery = new Address($order->id_address_delivery);
        $addressInvoice = new Address($order->id_address_invoice);

        if (null === $addressDelivery->id || true === $preferCustomerAddress) {
            $addressDelivery = array_filter(
                $addressCollection,
                function ($v) use ($customer) {
                    return $v->id_customer == $customer->id;
                }
            );

            if (is_array($addressDelivery) && 1 == count($addressDelivery)) {
                $addressDelivery = reset($addressDelivery);
            }
        }

        $addressBuilder = new KeycrmAddressBuilder();
        $addressBuilder
            ->setMode(KeycrmAddressBuilder::MODE_ORDER_DELIVERY)
            ->setAddress($addressDelivery)
            ->build()
        ;

        $addressBuilderRes = $addressBuilder->getDataArray();
        $crmOrder = array_merge($crmOrder, $addressBuilderRes);

        if (!empty($customer->email)){
            $crmOrder['buyer']['email'] = $customer->email;
        }

//        if ($addressInvoice instanceof Address && !empty($addressInvoice->company)) {
//            $crmOrder['contragent']['legalName'] = $addressInvoice->company;
//
//            if (!empty($addressInvoice->vat_number)) {
//                $crmOrder['contragent']['INN'] = $addressInvoice->vat_number;
//            }
//        }

        if (isset($payment[$paymentType]) && !empty($payment[$paymentType])) {
            $order_payment = [
//                'externalId' => $order->id . '#' . $order->reference,
                'payment_method_id' => (string)$payment[$paymentType],
            ];

            if (0 < round($order->total_paid_real, 2)) {
                $order_payment['amount'] = round($order->total_paid_real, 2);
            }
            else {
                $order_payment['amount'] = round($order->total_paid_tax_incl, 2);
            }
            $order_payment['status'] = 'not_paid';
            $crmOrder['payments'][] = $order_payment;
        } else {
            $crmOrder['payments'] = [];
        }

        $idCarrier = $dataFromCart ? $cart->id_carrier : $order->id_carrier;

        if (empty($idCarrier)) {
            $idCarrier = $order->id_carrier;
            $totalShipping = $order->total_shipping;
            $totalShippingWithoutTax = $order->total_shipping_tax_excl;
        } else {
            $totalShipping = $dataFromCart ? $cart->getCarrierCost($idCarrier) : $order->total_shipping;

            if (!empty($totalShipping) && 0 != $totalShipping) {
                $totalShippingWithoutTax = $dataFromCart
                    ? $totalShipping - $cart->getCarrierCost($idCarrier, false)
                    : $order->total_shipping_tax_excl;
            } else {
                $totalShippingWithoutTax = $order->total_shipping_tax_excl;
            }
        }

        // TODO Shouldn't cause any errors while creating order even if correspondent carrier is not set.
        if (array_key_exists($idCarrier, $delivery) && !empty($delivery[$idCarrier])) {
            $crmOrder['shipping']['delivery_service_id'] = (int)$delivery[$idCarrier];
        }

        if (isset($totalShipping) && $order->total_discounts > $order->total_products_wt) {
            $totalShipping -= $order->total_discounts - $order->total_products_wt;
            $crmOrder['discount_amount'] = round($order->total_products_wt, 2);
        } else {
            $crmOrder['discount_amount'] = round($order->total_discounts, 2);
        }

        if (isset($totalShipping) && 0 < round($totalShipping, 2)) {
            $crmOrder['shipping_price'] = round($totalShipping, 2);
        } else {
            $crmOrder['shipping_price'] = 0.00;
        }

//        if (isset($totalShippingWithoutTax) && 0 < round($totalShippingWithoutTax, 2)) {
//            $crmOrder['delivery']['netCost'] = round($totalShippingWithoutTax, 2);
//        } else {
//            $crmOrder['delivery']['netCost'] = 0.00;
//        }

        $comment = $order->getFirstMessage();

        if (false !== $comment) {
            $crmOrder['buyer_comment'] = $comment;
        }

        if ($dataFromCart) {
            $productStore = $cart;
            $converter = function ($product) {
                $map = [
                    'product_attribute_id' => 'id_product_attribute',
                    'product_quantity' => 'cart_quantity',
                    'product_id' => 'id_product',
                    'id_order_detail' => 'id_product',
                    'product_name' => 'name',
                    'product_price' => 'price',
                    'purchase_supplier_price' => 'price',
                    'product_price_wt' => 'price_wt',
                ];

                foreach ($map as $target => $value) {
                    if (isset($product[$value])) {
                        $product[$target] = $product[$value];
                    }
                }

                return $product;
            };
        } else {
            $productStore = $order;
            $converter = function ($product) {
                return $product;
            };
        }
        // todo !!!!!!!!!!! add product variation image (now - default image)

        $orderProducts = $productStore->getProducts();
        foreach ($orderProducts as $productData) {
            $product = $converter($productData);
            // Language id
            $id_lang = (int) Configuration::get('PS_LANG_DEFAULT');
            // Get cover image for your product
            $image = Image::getCover($product["product_id"]);
            // Load Product Object
            $temp_product = new Product($product["product_id"]);
            // Initialize the link object
            $link = new Link;
            $imagePath = $_SERVER["REQUEST_SCHEME"].'://' . $link->getImageLink($temp_product->link_rewrite[Context::getContext()->language->id], $image['id_image'], 'home_default');

            //далее фильтруем по id_product_attribute и добавляем в продукт


            if (isset($product['product_attribute_id']) && 0 < $product['product_attribute_id']) {
                $combinations = $temp_product->getAttributeCombinations((int)$id_lang, true);
                $productCombination = $temp_product->getAttributeCombinationsById($productData["product_attribute_id"], $id_lang, $groupByIdAttributeGroup = true);
                foreach ($productCombination as $pCombination) {
                    $properties[] = [
                        'name' => $pCombination["group_name"],
                        'value' => $pCombination['attribute_name'],
                    ];
                }

            } else {
                $productId = $product['product_id'];
            }



            /*----------------------------------------*/

            if (isset($product['attributes']) && $product['attributes']) {
                $arProp = [];
                $count = 0;
                $arAttr = explode(',', $product['attributes']);

                foreach ($arAttr as $valAttr) {
                    $arItem = explode(':', $valAttr);

                    if ($arItem[0] && $arItem[1]) {
                        // Product property code should start with a letter, digit or underscore
                        // and only contain letters, digits, underscores, hyphens and colons
                        $propertyCode = preg_replace('/(^[^\w]+)|([^\w\-:])/', '', $arItem[0]);
                        if (empty($propertyCode)) {
                            $propertyCode = 'prop_' . $count;
                        }

                        $arProp[$count]['code'] = $propertyCode;
                        $arProp[$count]['name'] = trim($arItem[0]);
                        $arProp[$count]['value'] = trim($arItem[1]);
                    }

                    ++$count;
                }
            }

            $item = [
                'sku' => !empty($product["product_supplier_reference"]) ? $product["product_supplier_reference"] : $product["product_reference"],
                'price' => round($product['price'], 2),
                'quantity' => (int)$product['product_quantity'],
                'name' => $product['product_name'],
                'picture' => $imagePath,
                'properties' => $properties,
//                'externalIds' => [
//                    [
//                        'code' => 'prestashop',
//                        'value' => $productId . '_' . $product['id_order_detail'],
//                    ],
//                ],
//                'offer' => ['externalId' => $productId],



                /*'initialPrice' => !empty($item['rate'])
                    ? $item['price'] + ($item['price'] * $item['rate'] / 100)
                    : $item['price'],*/
//                'purchasePrice' => round($product['purchase_supplier_price'], 2),
            ];

//            if (true == Configuration::get('PS_TAX') && isset($product['product_price_wt'])) {
//                $item['initialPrice'] = round($product['product_price_wt'], 2);
//            }

            if (isset($arProp)) {
                array_merge($item['properties'], $arProp);
            }

            $crmOrder['products'][] = $item;
        }

//        if ($order->gift && 0 < $order->total_wrapping) {
//            self::setOrderGiftItem($order, $crmOrder);
//        }

//        if ($order->id_customer) {
//            if (!empty($customerId)) {
//                $crmOrder['customer']['id'] = $customerId;
//            }
//
//            if (!empty($contactPersonExternalId)) {
//                $crmOrder['contact']['externalId'] = $contactPersonExternalId;
//                $crmOrder['contact']['site'] = $site;
//            } elseif (!empty($contactPersonId)) {
//                $crmOrder['contact']['id'] = $contactPersonId;
//                $crmOrder['contact']['site'] = $site;
//            }
//
//            if (!empty($site)) {
//                $crmOrder['customer']['site'] = $site;
//            }
//
//            if (KeycrmTools::isCorporateEnabled() && KeycrmTools::isOrderCorporate($order)) {
//                $crmOrder['contragent']['contragentType'] = 'legal-entity';
//            } else {
//                $crmOrder['contragent']['contragentType'] = 'individual';
//            }
//        }
        return $crmOrder;
//        return KeycrmTools::filter(
//            'KeycrmFilterProcessOrder',
//            KeycrmTools::clearArray($crmOrder),
//            [
//                'order' => $order,
//                'customer' => $customer,
//                'cart' => $cart,
//            ]);
    }

    /**
     * Build array with order data for keyCRM from PrestaShop cart data
     *
     * @param \KeycrmProxy|\KeycrmApiClientV5 $api
     * @param Cart $cart Cart with data
     * @param string $externalId External ID for order
     * @param string $paymentType Payment type (buildCrmOrder requires it)
     * @param string $status Status for order
     *
     * @return array
     *
     * @throws \Exception
     */
    public static function buildCrmOrderFromCart($api, $cart = null, $externalId = '', $paymentType = '', $status = '')
    {
        if (empty($cart) || empty($paymentType) || empty($status)) {
            return [];
        }

        try {
            $order = new Order();
            $order->id_cart = $cart->id;
            $order->id_customer = $cart->id_customer;
            $order->id_address_delivery = $cart->id_address_delivery;
            $order->id_address_invoice = $cart->id_address_invoice;
            $order->id_currency = $cart->id_currency;
            $order->id_carrier = $cart->id_carrier;
            $order->total_discounts = $cart->getOrderTotal(true, Cart::ONLY_DISCOUNTS);
            $order->module = $paymentType;
            $order->payment = $paymentType;

            if (!empty($cart->id_carrier)) {
                $order->total_shipping = $cart->getPackageShippingCost();
                $order->total_shipping_tax_excl = $cart->getPackageShippingCost(null, false);
            }

            $orderBuilder = new KeycrmOrderBuilder();
            $orderData = $orderBuilder
                ->defaultLangFromConfiguration()
                ->setApi($api)
                ->setCmsOrder($order)
                ->setCmsCart($cart)
                ->setCmsCustomer(new Customer($cart->id_customer))
                ->buildOrderWithPreparedCustomer(true)
            ;
            $orderData['externalId'] = $externalId;
            $orderData['status'] = $status;

            unset($orderData['payments']);

            return KeycrmTools::clearArray($orderData);
        } catch (\InvalidArgumentException $exception) {
            KeycrmLogger::writeCaller(
                'buildCrmOrderFromCart',
                $exception->getMessage()
            );

            return [];
        }
    }

    /**
     * Builds keyCRM customer data from PrestaShop customer data
     *
     * @param Customer $object
     * @param array $address
     *
     * @return array
     */
    public static function buildCrmCustomer(Customer $object, $address = [])
    {
        $customer = array_filter(array_merge(
            [
                'externalId' => !empty($object->id) ? $object->id : null,
                'firstName' => $object->firstname,
                'lastName' => $object->lastname,
                'email' => $object->email,
                'subscribed' => $object->newsletter,
                'createdAt' => KeycrmTools::verifyDate($object->date_add, 'Y-m-d H:i:s')
                    ? $object->date_add : date('Y-m-d H:i:s'),
                'birthday' => KeycrmTools::verifyDate($object->birthday, 'Y-m-d')
                    ? $object->birthday : '',
                'sex' => '1' == $object->id_gender ? 'male' : ('2' == $object->id_gender ? 'female' : ''),
            ],
            $address
        ), function ($value) {
            return !('' === $value || null === $value || (is_array($value) ? 0 == count($value) : false));
        });

        return KeycrmTools::filter(
            'KeycrmFilterProcessCustomer',
            $customer,
            [
                'customer' => $object,
                'address' => $address,
            ]);
    }

    public static function buildCrmCustomerCorporate(
        Customer $object,
        $nickName = '',
        $contactExternalId = '',
        $appendAddress = false,
        $appendCompany = false,
        $site = ''
    ) {
        $customerAddresses = [];
        $addresses = $object->getAddresses((int) Configuration::get('PS_LANG_DEFAULT'));
        $customer = [
            'addresses' => [],
            'companies' => [],
        ];
        $company = [
            'isMain' => true,
            'externalId' => null,
            'active' => true,
            'name' => '',
        ];

        // TODO: $company['contragent']['INN'] may not work, should check that later...
        foreach ($addresses as $address) {
            $addressBuilder = new KeycrmAddressBuilder();

            if ($address instanceof Address && !empty($address->company)) {
                $customerAddresses[] = $addressBuilder
                    ->setMode(KeycrmAddressBuilder::MODE_CORPORATE_CUSTOMER)
                    ->setAddress($address)
                    ->setWithExternalId(true)
                    ->build()
                    ->getDataArray()
                ;
                $customer['nickName'] = empty($nickName) ? $address->company : $nickName;
                $company['name'] = $address->company;
                $company['contragent']['INN'] = $address->vat_number;
                $company['externalId'] = 'company_' . $address->id;
            }

            if (is_array($address) && !empty($address['company'])) {
                $customerAddresses[] = $addressBuilder
                    ->setMode(KeycrmAddressBuilder::MODE_CORPORATE_CUSTOMER)
                    ->setAddressId($address['id_address'])
                    ->setWithExternalId(true)
                    ->build()
                    ->getDataArray()
                ;
                $customer['nickName'] = empty($nickName) ? $address->company : $nickName;
                $company['name'] = $address['company'];
                $company['contragent']['INN'] = $address['vat_number'];
                $company['externalId'] = 'company_' . $address['id_address'];
            }
        }

        if ($appendCompany && null !== $company['externalId']) {
            $customer['companies'][] = $company;
        }

        if (!empty($contactExternalId) && !empty($site)) {
            $customer['customerContacts'] = [[
                'isMain' => true,
                'customer' => [
                    'externalId' => $contactExternalId,
                    'site' => $site,
                ],
            ]];

            if (!empty($customer['companies'])
                && isset($customer['companies'][0], $customer['companies'][0]['externalId'])
            ) {
                $customer['customerContacts'][0]['companies'] = [[
                    'company' => ['externalId' => $customer['companies'][0]['externalId']],
                ]];
            }
        }

        if ($appendAddress) {
            $customer['addresses'] = $customerAddresses;
        }

        return KeycrmTools::filter(
            'KeycrmFilterProcessCustomerCorporate',
            KeycrmTools::clearArray($customer),
            [
                'customer' => $object,
            ]);
    }

    /**
     * Returns true if provided item array contains placeholder item added for equal price with payment.
     *
     * @param array $item
     *
     * @return bool
     */
    public static function isGiftItem($item)
    {
        if (isset($item['offer'], $item['offer']['externalId'])
            && KeycrmReferences::GIFT_WRAPPING_ITEM_EXTERNAL_ID == $item['offer']['externalId']
        ) {
            return true;
        }

        if (isset($item['externalIds'])) {
            foreach ($item['externalIds'] as $externalId) {
                if ('prestashop' == $externalId['code']
                    && KeycrmReferences::GIFT_WRAPPING_ITEM_EXTERNAL_ID == $externalId['value']
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Returns gift item
     *
     * @param float $giftItemPrice
     *
     * @return array
     */
    public static function getGiftItem($giftItemPrice)
    {
        return [
            'externalIds' => [[
                'code' => 'prestashop',
                'value' => KeycrmReferences::GIFT_WRAPPING_ITEM_EXTERNAL_ID,
            ]],
            'offer' => ['externalId' => KeycrmReferences::GIFT_WRAPPING_ITEM_EXTERNAL_ID],
            'productName' => 'Gift Wrapping Cost',
            'quantity' => 1,
            'initialPrice' => $giftItemPrice,
            'purchasePrice' => $giftItemPrice,
        ];
    }

    /**
     * Sets gift item to order (should be called if order is marked as gift)
     *
     * @param Order|\OrderCore $orderCms
     * @param array $orderCrm
     */
    private static function setOrderGiftItem($orderCms, &$orderCrm)
    {
        $isFound = false;
        $giftItemPrice = round($orderCms->total_wrapping, 2);

        foreach ($orderCrm['items'] as $key => $item) {
            if (self::isGiftItem($item)) {
                $orderCrm['items'][$key] = self::getGiftItem($giftItemPrice);
                $isFound = true;

                break;
            }
        }

        if (!$isFound) {
            $orderCrm['items'][] = self::getGiftItem($giftItemPrice);
        }
    }
}
