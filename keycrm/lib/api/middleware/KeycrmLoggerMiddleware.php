<?php
/**
 * MIT License
 *
 * Copyright (c) 2021 GetBetter OU
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    GetBetter OU <support@keycrm.app>
 *  @copyright 2021 GetBetter OU
 *  @license   https://opensource.org/licenses/MIT  The MIT License
 *
 * Don't forget to prefix your containers with your own identifier
 * to avoid any conflicts with others containers.
 */

class KeycrmLoggerMiddleware implements KeycrmMiddlewareInterface
{
    /**
     * {@inheritDoc}
     */
    public function __invoke(KeycrmApiRequest $request, callable $next = null)
    {
        $method = $request->getMethod();

        if (null !== $method) {
            KeycrmLogger::writeDebug($method, print_r($request->getData(), true));
        }

        /** @var KeycrmApiResponse $response */
        $response = $next($request);

        if ($response->isSuccessful()) {
            // Don't print long lists in debug logs (errors while calling this will be easy to detect anyway)
            if (in_array($method, ['statusesList', 'paymentTypesList', 'deliveryTypesList'])) {
                KeycrmLogger::writeDebug($method, '[request was successful, but response is omitted]');
            } else {
                KeycrmLogger::writeDebug($method, $response->getRawResponse());
            }
        } else {
            if ($response->offsetExists('errorMsg')) {
                KeycrmLogger::writeCaller($method, $response['errorMsg']);
            }

            if ($response->offsetExists('errors')) {
                KeycrmApiErrors::set($response['errors'], $response->getStatusCode());
                $error = KeycrmLogger::reduceErrors($response['errors']);
                KeycrmLogger::writeNoCaller($error);
            }
        }

        return $response;
    }
}
